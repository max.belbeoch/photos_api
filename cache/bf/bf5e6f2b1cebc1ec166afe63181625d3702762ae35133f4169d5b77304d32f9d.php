<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/shop.php */
class __TwigTemplate_ff5e7b83b37e23f037e8bc837b9a16bfbc615a15bc78ac85825821e6ff9d77e4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
    <div class=\"container-fluid\">

<br><br>
<h3>Récap Stripe + liste photos sur la boutique (trafic etc)</h3>
        <div class=\"row\">
            <div class=\"col-sm-3 bg-white\">
            ";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getRuntime('Slim\Views\TwigRuntimeExtension')->getBasePath(), "html", null, true);
        echo "
            </div>
        </div>

    </div>";
    }

    public function getTemplateName()
    {
        return "admin/shop.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 8,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin/shop.php", "/Applications/MAMP/htdocs/photos_api/templates/admin/shop.php");
    }
}
