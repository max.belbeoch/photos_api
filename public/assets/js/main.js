/*
	Astral by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
*/

(function ($) {

	var $window = $(window),
		$body = $('body'),
		$wrapper = $('#wrapper'),
		$main = $('#main'),
		$panels = $main.children('.panel'),
		$nav = $('#nav'), $nav_links = $nav.children('a');

	// Breakpoints.
	breakpoints({
		xlarge: ['1281px', '1680px'],
		large: ['981px', '1280px'],
		medium: ['737px', '980px'],
		small: ['361px', '736px'],
		xsmall: [null, '360px']
	});

	// Play initial animations on page load.
	$window.on('load', function () {
		window.setTimeout(function () {

			FlexMasonry.init('.grid', {
				responsive: true,
				breakpointCols: {
					'min-width: 1500px': 3,
					'min-width: 1200px': 3,
					'min-width: 992px': 2,
					'min-width: 768px': 2,
					'min-width: 576px': 1,
				}
				//numCols: 4
			});

		}, 100);
	});

	// Nav.
	$nav_links
		.on('click', function (event) {

			var href = $(this).attr('href');

			// Not a panel link? Bail.
			if (href.charAt(0) != '#'
				|| $panels.filter(href).length == 0)
				return;

			// Prevent default.
			event.preventDefault();
			event.stopPropagation();

			// Change panels.
			if (window.location.hash != href)
				window.location.hash = href;

		});

	// Panels.

	// Initialize.
	(function () {

		var $panel, $link;

		// Get panel, link.
		if (window.location.hash) {

			$panel = $panels.filter(window.location.hash);
			$link = $nav_links.filter('[href="' + window.location.hash + '"]');

		}

		// No panel/link? Default to first.
		if (!$panel
			|| $panel.length == 0) {

			$panel = $panels.first();
			$link = $nav_links.first();

		}

		// Deactivate all panels except this one.
		$panels.not($panel)
			.addClass('inactive')
			.hide();

		// Activate link.
		$link
			.addClass('active');

		// Reset scroll.
		$window.scrollTop(0);

	})();

	// Hashchange event.
	$window.on('hashchange', function (event) {

		var $panel, $link;

		// Get panel, link.
		if (window.location.hash) {

			$panel = $panels.filter(window.location.hash);
			$link = $nav_links.filter('[href="' + window.location.hash + '"]');

			// No target panel? Bail.
			if ($panel.length == 0)
				return;

		}

		// No panel/link? Default to first.
		else {

			$panel = $panels.first();
			$link = $nav_links.first();

		}

		// Deactivate all panels.
		$panels.addClass('inactive');

		// Deactivate all links.
		$nav_links.removeClass('active');

		// Activate target link.
		$link.addClass('active');

		// Set max/min height.
		$main
			.css('max-height', $main.height() + 'px')
			.css('min-height', $main.height() + 'px');

		// Delay.
		setTimeout(function () {

			// Hide all panels.
			$panels.hide();

			// Show target panel.
			$panel.show();

			// Set new max/min height.
			$main
				.css('max-height', $panel.outerHeight() + 'px')
				.css('min-height', $panel.outerHeight() + 'px');

			// Reset scroll.
			$window.scrollTop(0);

			// Delay.
			window.setTimeout(function () {

				// Activate target panel.
				$panel.removeClass('inactive');

				// Clear max/min height.
				$main
					.css('max-height', '')
					.css('min-height', '');

				// IE: Refresh.
				$window.triggerHandler('--refresh');

				// Unlock.
				locked = false;

			}, (breakpoints.active('small') ? 0 : 500));

		}, 250);

	});

	// IE: Fixes.
	if (browser.name == 'ie') {

		// Fix min-height/flexbox.
		$window.on('--refresh', function () {

			$wrapper.css('height', 'auto');

			window.setTimeout(function () {

				var h = $wrapper.height(),
					wh = $window.height();

				if (h < wh)
					$wrapper.css('height', '100vh');

			}, 0);

		});

		$window.on('resize load', function () {
			$window.triggerHandler('--refresh');
		});

		// Fix intro pic.
		$('.panel.intro').each(function () {

			var $pic = $(this).children('.pic'),
				$img = $pic.children('img');

			$pic
				.css('background-image', 'url(' + $img.attr('src') + ')')
				.css('background-size', 'cover')
				.css('background-position', 'center');

			$img
				.css('visibility', 'hidden');

		});

	}

})(jQuery);


// lightbox handlure

function hidelightbox() {
	console.log('in');
	for (let i = 0; i < lightbox.length; i++) {
		lightbox[i].style.display = 'hidden';
	}
}

// set menu active

function setActive(page) {
	var links = document.getElementsByClassName("btn_menu_perso");
	var arr = Array.prototype.slice.call(links)

	arr.forEach(elem => {
		elem.classList.remove('btn_menu_perso_active');
	});

	switch (page) {
		case 'portfolio':
			document.getElementById('portfolio_btn').classList.add('btn_menu_perso_active');
			break;
		case 'albums':
		case 'album':
			document.getElementById('albums_btn').classList.add('btn_menu_perso_active');
			break;
		case 'propos':
			document.getElementById('propos_btn').classList.add('btn_menu_perso_active');
			break;
		case 'contact':
			document.getElementById('contact_btn').classList.add('btn_menu_perso_active');
			break;
	}
}

async function displayImg(id) {

	document.getElementById('img_light').innerHTML = '<div class="loader">Chargement ...</div>';

	await $.get('http://localhost:8888/photos_api/photo/' + id + '/1/0/1',
		function (data) {
			document.getElementById('img_light').innerHTML = '<img class="image _bq" src="data:image/jpeg;base64,' + data + '" alt="Protest" />'
		}
	);
}

function validateFields() {

	let contact_name = document.getElementById('contact_name').value;
	let contact_mail = document.getElementById('contact_mail').value;
	let contact_subject = document.getElementById('contact_subject').value;
	let contact_message = document.getElementById('contact_message').value;
	let token = document.getElementById('token').value;

	if (contact_name.length < 1) {
		document.getElementById('contact_name').classList.add("is-invalid");
		scrollTop();
		return false;
	} else {
		document.getElementById('contact_name').classList.remove("is-invalid");
	}

	var verif = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

	if (contact_mail.length < 1) {
		document.getElementById('contact_mail').classList.add("is-invalid");
		scrollTop();
		return false;
	} else if (!verif.test(String(contact_mail).toLowerCase())) {
		document.getElementById('contact_mail').classList.add("is-invalid");
		scrollTop();
		return false;
	} else {
		document.getElementById('contact_mail').classList.remove("is-invalid");
	}

	if (contact_subject.length < 1) {
		document.getElementById('contact_subject').classList.add("is-invalid");
		scrollTop();
		return false;
	} else {
		document.getElementById('contact_subject').classList.remove("is-invalid");
	}

	if (contact_message.length < 1) {
		document.getElementById('empty_message').style.display = 'block';
		return false;
	} else {
		document.getElementById('empty_message').style.display = 'none';
	}

	if (token.length < 1) {
		document.getElementById('empty_token').style.display = 'block';
		return false;
	} else {
		document.getElementById('empty_token').style.display = 'none';
	}

	return true;
}

function submitContactForm() {

	if (!validateFields()) {
		return;
	}

	document.getElementById('contact_form_submit').disabled = true;

	let contact_name = document.getElementById('contact_name').value;
	let contact_mail = document.getElementById('contact_mail').value;
	let contact_subject = document.getElementById('contact_subject').value;
	let contact_message = document.getElementById('contact_message').value;
	let token = document.getElementById('token').value;

	$.ajax({
		url: 'contact',
		type: 'POST',
		data: {
			'g-recaptcha-response': token,
			'contact_name': contact_name,
			'contact_mail': contact_mail,
			'contact_subject': contact_subject,
			'contact_message': contact_message,
		},
		dataType: 'json',
		success: function (data) {

			if (data.result) {
				document.getElementById('success_message').innerHTML = data.message;
				document.getElementById('success_message').style.display = 'block';
				setTimeout(function () { location.reload() }, 3000);
			} else {
				document.getElementById('error_message').innerHTML = data.message;
				document.getElementById('error_message').style.display = 'block';
				document.getElementById('contact_form_submit').disabled = false;
			}
		}
	});
}

function scrollTop() {
	$("html, body").animate(
		{ scrollTop: "0" }, 1000);
}