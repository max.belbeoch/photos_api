// Pictures
function showUpdateModal(id) {

    $.ajax({
        url: '/photos_api/api/picture/' + id,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            if (data.result) {
                let img = data.picture;
                document.getElementById('idImgToUpdate').value = id;
                document.getElementById('imgName').innerHTML = img.nom_photo;
                document.getElementById('newNom').value = img.nom;
                document.getElementById('newAlbumId').value = img.album_id;
                document.getElementById('newLocalisation').value = img.localisation;
                document.getElementById('newNote').value = img.note;
                document.getElementById('newToSell').checked = img.to_sell == 1 ? true : false;
                $("#updateModal").modal('toggle');
            } else {
                let div = '<div class="alert alert-danger">' + data.message + '</div>';
                $('#alert_container').append(div);
            }
        }
    });
}

function updatePic() {

    let id = document.getElementById('idImgToUpdate').value;
    let nom = document.getElementById('newNom').value;
    let albumId = document.getElementById('newAlbumId').value;
    let localisation = document.getElementById('newLocalisation').value;
    let note = document.getElementById('newNote').value;
    let description = document.getElementById('newDescription').value;
    let toSell = document.getElementById('newToSell').checked ? 1 : 0;

    $.ajax({
        url: 'admin/picture/update',
        type: 'POST',
        data: {
            'id': id,
            'newNom': nom,
            'newAlbumId': albumId,
            'newLocalisation': localisation,
            'newNote': note,
            'newToSell': toSell,
            'newDescription': description,
        },
        dataType: 'json',
        success: function (data) {
            if (data.result) {
                var div = '<div class="alert alert-success">Modifiée !</div>';
            } else {
                var div = '<div class="alert alert-danger">Une erreur est survenue</div>';
            }
            $('#alert_container').append(div);
            $("#updateModal").modal('toggle');
            setTimeout(function () { location.reload() }, 1000);
        }
    });
}

function deletePic(id) {

    $.ajax({
        url: 'admin/picture/delete',
        type: 'POST',
        data: {
            'id': id
        },
        dataType: 'json',
        success: function (data) {
            if (data.result) {
                var div = '<div class="alert alert-success">Supprimée</div>';
            } else {
                var div = '<div class="alert alert-danger">Une erreur est survenue</div>';
            }
            $('#alert_container').append(div);
            setTimeout(function () { location.reload() }, 1000);
        }
    });
}

// Parameters
function showUpdateParameterModal(id) {

    $.ajax({
        url: '/photos_api/admin/picture_parameter/' + id,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            if (data.result) {
                let param = data.parameter;
                document.getElementById('idParameterToUpdate').value = id;
                document.getElementById('parameter_label').innerHTML = param.label;
                document.getElementById('new_label').value = param.label;
                document.getElementById('new_width').value = param.width;
                document.getElementById('new_height').value = param.height;
                document.getElementById('new_price').value = param.price;
                document.getElementById('new_print').value = param.print;
                $("#updateParameterModal").modal('toggle');
            } else {
                let div = '<div class="alert alert-danger">' + data.message + '</div>';
                $('#alert_container').append(div);
            }
        }
    });
}

function updateParameter() {

    let id = document.getElementById('idParameterToUpdate').value;
    let label = document.getElementById('new_label').value;
    let width = document.getElementById('new_width').value;
    let height = document.getElementById('new_height').value;
    let price = document.getElementById('new_price').value;
    let print = document.getElementById('new_print').value;

    $.ajax({
        url: 'admin/picture_parameter/update',
        type: 'POST',
        data: {
            'id': id,
            'label': label,
            'width': width,
            'height': height,
            'price': price,
            'print': print,
        },
        dataType: 'json',
        success: function (data) {
            if (data.result) {
                var div = '<div class="alert alert-success">Modifiée !</div>';
            } else {
                var div = '<div class="alert alert-danger">Une erreur est survenue</div>';
            }
            $('#alert_container').append(div);
            $("#updateParameterModal").modal('toggle');
            setTimeout(function () { location.reload() }, 1000);
        }
    });
}

function deletePicParameter(id) {

    $.ajax({
        url: 'admin/picture_parameter/delete',
        type: 'POST',
        data: {
            'id': id
        },
        dataType: 'json',
        success: function (data) {
            if (data.result) {
                var div = '<div class="alert alert-success">Supprimée</div>';
            } else {
                var div = '<div class="alert alert-danger">Une erreur est survenue</div>';
            }
            $('#alert_container').append(div);
            setTimeout(function () { location.reload() }, 1000);
        }
    });
}


// Picture parameters
function showItemModal(id) {

    $('#parameters_list').html('');
    
    $.ajax({
        url: '/photos_api/admin/picture_items/' + id,
        type: 'GET',
        dataType: 'json',
        success: function (data) { 
            if (data.result) {
                
                let params = data.parameters;
                var options = '';
                document.getElementById('idPictureToSet').value = id;

                params.forEach(elem => {
                    let check = '';
                    elem.checked == 1 ? check = 'checked=""' : check = '';
                    options += '<input type="checkbox" name="picture_parameters" id="' + elem.id + '" value="' + elem.id + '" ' + check + '> <label for="' + elem.id + '">  ' + elem.print + ' <b>-</b> ' + elem.label + ' <b>-</b> ' + elem.price + '€</label><br>'
                });
                $('#parameters_list').append(options);
                $("#setItemModal").modal('toggle');
            } else {
                let div = '<div class="alert alert-danger">' + data.message + '</div>';
                $('#alert_container').append(div);
            }
        }
    });
}

function updateItem() {

    let id = document.getElementById('idPictureToSet').value;
    let items = document.getElementsByName("picture_parameters");
    let data = [];

    items.forEach(item => {
        data.push({
            'parameter_id': item.value,
            'checked': item.checked ? 1 : 0
        });
    })

    $.ajax({
        url: 'admin/picture_item/set',
        type: 'POST',
        data: {
            'id': id,
            'data': data
        },
        dataType: 'json',
        success: function (data) {
            if (data.result) {
                var div = '<div class="alert alert-success">Paramétrage modifié !</div>';
            } else {
                var div = '<div class="alert alert-danger">Une erreur est survenue</div>';
            }
            $('#alert_container').append(div);
            $("#setItemModal").modal('toggle');
            setTimeout(function () { location.reload() }, 1000);
        },
        error: (err) => {
            console.log(err)
        }
    });
}