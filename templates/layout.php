<!DOCTYPE HTML>
<html style="overflow-x: hidden; overflow-y: hidden;">

<?php

if (isset($_SESSION['token'])) : ?>

    <div style="position: absolute;">
        <a href="admin/gestion.php" class="alert-link"> gestion</a> |
        <a href="admin/deconnexion.php" class="alert-link">déconnexion</a>
    </div>

<?php endif; ?>


<head>
    <title>Maxime Belbeoch</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <base href="http://localhost:8888/photos_api/" />
    <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/main.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/style_perso.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/style_animation.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/responsive_perso.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/flexmasonry.css" />

    <noscript>
        <link rel="stylesheet" href="assets/css/noscript.css" />
    </noscript>

    <div id="loadingDiv">
        <div class="loader">Chargement ...</div>
    </div>
</head>

<body id="body">

    <!-- Scripts -->
    <script type="text/javascript" src="assets/js/flexmasonry.js"></script>
    <script type="text/javascript" src="assets/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/browser.min.js"></script>
    <script type="text/javascript" src="assets/js/breakpoints.min.js"></script>
    <script type="text/javascript" src="assets/js/util.js"></script>
    <script type="text/javascript" src="assets/js/main.js?random=<?= uniqid(); ?>"></script>
    <script type="text/javascript" src="assets/js/popper.min.js"></script>

    <!-- Wrapper-->
    <div id="wrapper" style="opacity:0;">

        <?php

        if (!isset($route)) : ?>

            <h1>Maxime Belbeoch</h1>

            <nav class="navbar navbar-expand-lg navbar-light bg-light navbar_perso">
                <button class="navbar-toggler menu_btn" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 ul_perso">
                        <li class="nav-item li_perso">
                            <a class="btn_menu_perso btn_menu_perso_active" id="portfolio_btn" href="portfolio">
                                <span>
                                    <span>
                                        <span>Portfolio</span>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <li class="nav-item li_perso">
                            <a class="btn_menu_perso" href="albums" id="albums_btn">
                                <span>
                                    <span>
                                        <span>Albums</span>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <li class="nav-item li_perso">
                            <a class="btn_menu_perso" href="propos" id="propos_btn">
                                <span>
                                    <span>
                                        <span>À propos</span>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <li class="nav-item li_perso">
                            <a class="btn_menu_perso" href="contact" id="contact_btn">
                                <span>
                                    <span>
                                        <span>Contact</span>
                                    </span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

        <?php endif; ?>

        <?= $content ?>

        <div id="footer">
            <ul class="copyright">
                <li> &copy; &nbsp;&nbsp; Maxime Belbeoch</li>
            </ul>
        </div>
    </div>

</body>

<script>
    setActive('<?= $page ?>');

    $(window).on('load', function() {
        setTimeout(removeLoader, 1000); //wait for page load PLUS two seconds.
    });

    function removeLoader() {

        $("#wrapper").css('opacity', '1');
        $("#body").css('opacity', '1');
        $('html').css('overflow-y', 'visible');

        $("#loadingDiv").fadeOut(1000, function() {
            $("#loadingDiv").remove(); //makes page more lightweight 
        });
    }
</script>

</html>