<!-- Main -->
<div id="main">

    <!-- Contact -->
    <article id="contact" class="panel">
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer>
        </script>

        <form action="" method="POST" style="margin-bottom: 0;">
            <input type="hidden" id="token" name="token" value="">
            <div class="invalid-feedback">
                Le nom est obligatoire.
            </div>
            <div class="form-group row" style="padding: 1% 2% 0% 2%;">
                <label class="col-sm-1" for="contact_name" style="top: 12%;">Nom</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="Nom" required>
                    <div class="invalid-feedback">
                        Le nom est obligatoire.
                    </div>
                </div>
                <label class="col-sm-1" ≤ for="contact_mail" style="top: 12%;">Email</label>
                <div class="col-sm-5">
                    <input type="email" class="form-control" id="contact_mail" name="contact_mail" placeholder="Email" required>
                    <div class="invalid-feedback">
                        Veuillez vérifier votre adresse mail.
                    </div>
                </div>
            </div>
            <div class="form-group row" style="padding: 1% 2% 0% 2%;">
                <label class="col-sm-1" for="contact_subject" style="top: 12%;">Sujet</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="contact_subject" name="contact_subject" placeholder="Sujet" required>
                    <div class="invalid-feedback">
                        Le sujet est obligatoire.
                    </div>
                </div>
            </div>
            <div class="form-check">
                <label for="contact_message">Message (max 500 caractères)</label>
                <textarea name="contact_message" id="contact_message" placeholder="Message" rows="3" maxlength="500"></textarea>
                <div class="invalid-feedback" id="empty_message" style="display: none">
                    Le message ne peut être vide.
                </div>
            </div>
        </form>

        <center>
            <br>
            <div class="invalid-feedback" id="empty_token" style="display: none">
                merci de valider le captcha
            </div>
            <div id="cap"></div>
            <br>
            <input type="button" id="contact_form_submit" onclick="submitContactForm()" value="Envoyer">
        </center>
        </form>

        <br>
        
        <center>
            <div class="alert alert-danger" role="alert" id="error_message" style="width: 50%; display: none;"></div>
        </center>
        <center>
            <div class="alert alert-success" role="alert" id="success_message" style="width: 50%; display: none;"></div>
        </center>

    </article>

</div>

<style>
    label {
        color: white !important;
    }
</style>

<script>
    var verifyCallback = function(response) {
        document.getElementById('token').value = response;
    };

    var onloadCallback = function() {
        grecaptcha.render('cap', {
            'sitekey': '6Lc7vIAcAAAAAB6itMXTadtkYARh47AQips7CFpL',
            'callback': verifyCallback
        });
    };
</script>