<!-- Main -->
<div id="main">

	<!-- Me -->
	<article id="home" class="panel intro">
		<header>
			<h1>Maxime Belbeoch</h1><br>
			<p class="home_subtitle">Développement web&nbsp; &middot; &nbsp;Photographie</p>
			<div class="opening_span">
				<section class="portfolio-experiment">
					<a href="portfolio">
						<span class="text">Entrer</span>
						<span class="line -right"></span>
						<span class="line -top"></span>
						<span class="line -left"></span>
						<span class="line -bottom"></span>
					</a>
				</section>
			</div>
		</header>
		<div class="intro_container">
			<img src="assets/sample_img/home-img-l.jpg" class="img_intro" id="img_intro_l" alt="" />
			<img src="assets/sample_img/home-img-s.jpg" class="img_intro" id="img_intro_s" alt="" />
		</div>
	</article>

</div>