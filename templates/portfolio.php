<section>

    <div class="container-fluid">
        <div class="row">
            <?php foreach ($images as $key => $image) : ?>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="overlay-image _bp">
                        <a href="portfolio#img_light" onclick="displayImg(<?= $image['id'] ?>)">
                            <img src="assets/upload/<?= $image["nom_photo_min"] ?>" style="width: 100%;">
                            <div class="hover _br">
                                <div class="text _q"><?= $image["nom"] ?></div>
                            </div>
                        </a>
                    </div>
                    <a href="portfolio#_" class="lightbox" id="img_light">
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

</section>