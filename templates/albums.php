
<div class="row">

<?php foreach ($albums as $album) : ?>

    <div class="col-lg-4 col-md-6 col-sm-12" style="padding:1%">
        <center>
            <figure class="snip1273">
                <img src="assets/picAlbums/<?= $album['nom'] ?>.jpg" alt="<?= $album['nom'] ?>" />
                <figcaption>
                    <h3><?= $album['nom'] ?></h3>
                </figcaption>
                <a href="album/<?= $album['id'] ?>"></a>
            </figure>
        </center>
    </div>

<?php endforeach; ?>

</div>