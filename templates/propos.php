<div class="row">

    <div class="col-lg-4 col-md-12">
        <img src="assets/sample_img/propos-img.jpg" class="img_propos" alt="" />
    </div>
    <div class="col-lg-8 col-md-12">
        <div id="main" class="main_propos">
            <article id="home" class="panel intro panel_propos">
                <header class="header_propos">
                    <p class="propos_text">
                        Breton expatrié au Pays Basque pour les études, je réalisais déjà quelques photos, mais de manière plutôt anodine, sans vouloir forcément les partager ou en faire
                        de vraies créations. Puis, il y a 2/3 ans, je me suis mis à photographier les couchers de soleil sur la côte Basque et ses superbes paysages.
                        J’ai alors commencé à m’y investir réellement et un déclic est survenu peu après … Un ami à moi m’a fait découvrir l’astro-photographie sur les plages landaises
                        et m’a par la suite emmené en randonnée dans les Pyrénées. En réalisant mon premier bivouac montagnard, je suis tombé amoureux des paysages alpins et de leurs
                        nuits uniques.
                    </p> <br>
                    <p class="propos_text">J’ai alors eu envie de capturer ces moments magiques qu’on ne vit qu’en dehors de nos conforts modernes et de les partager autour de moi.
                        Allongé sur un rocher, au sommet d’une montagne, à contempler les étoiles pendant des heures ou sur une plage déserte à contempler le soleil disparaître à
                        l’horizon , quel meilleur moyen pour retranscrire (un minimum) ces visions uniques si ce n’est, la photo ?
                    </p>
                </header>
            </article>

        </div>
    </div>
</div>