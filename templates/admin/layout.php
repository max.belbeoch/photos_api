<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <base href="http://localhost:8888/photos_api/" />
    <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css?1" />
    <link rel="stylesheet" type="text/css" href="assets/css/admin.css" />

    <title>Admin</title>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="display: inline-block; width: 100%;">
        <h3 style="float: right; color: #b7b7b7;"> MB photos - panel admin </h3><br><br>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="http://localhost:8888/photos_api/">Accueil site</a> &nbsp;&nbsp; | &nbsp;&nbsp;
                <a class="nav-item nav-link <?= $route == 'admin_home' ? 'active' : ''; ?>" href="http://localhost:8888/photos_api/admin/home">Home<span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link <?= $route == 'admin_shop' ? 'active' : ''; ?>" href="http://localhost:8888/photos_api/admin/shop">Shop</a>
                <a class="nav-item nav-link <?= $route == 'admin_order' ? 'active' : ''; ?>" href="http://localhost:8888/photos_api/admin/orders">Commandes</a>
                <a class="nav-item nav-link disabled"></a>
            </div>
        </div>
    </nav>

</head>

<body class="">

    <?= $content ?>

    <script type="text/javascript" src="assets/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/admin.js"></script>

</body>


</html>