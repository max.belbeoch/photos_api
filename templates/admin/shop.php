<div class="container-fluid">

    <br>

    <div class="col-md-6 mx-auto text-center" id="alert_container">

        <?php

        use App\Repository\PictureParameterRepository;

        if (isset($succesAjout)) : ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert alert-success alert-dismissible fade show"><?= $succesAjout ?></div>
        <?php elseif (isset($erreurAjout)) : ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert alert-danger alert-dismissible fade show"><?= $erreurAjout ?></div>
        <?php endif; ?>
    </div>

    <br>

    <div id="updateParameterModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="parameter_label"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form method="POST" enctype="multipart/form-data">
                        <input type="hidden" id="idParameterToUpdate" value="">
                        <div class="form-group">
                            <label for="new_label">Label</label>
                            <input type="text" id="new_label" name="new_label" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="new_width">Largeur</label>
                            <input type="number" id="new_width" name="new_width" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="new_height">Hauteur</label>
                            <input type="number" id="new_height" name="new_height" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="new_price">Prix</label>
                            <input type="number" id="new_price" name="new_price" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="new_print">Support d'impression</label>
                            <select name="new_print" id="new_print" class="form-control">
                                <?php foreach (PictureParameterRepository::PRINT as $key => $label) : ?>
                                    <option value="<?= $key ?>"><?= $label ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input class="btn btn-success" type="submit" value="Modifier" onclick="updateParameter()">
                </div>
            </div>
        </div>
    </div>

    <div id="setItemModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Choix des formats</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="idPictureToSet" value="">
                    <div id="parameters_list">
                    </div>
                </div>
                <div class="modal-footer">
                    <input class="btn btn-success" type="submit" value="Enregistrer" onclick="updateItem()">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-2 col-lg-6 col-md-4">

            <h3>Ajouter un paramétrage</h3>
            <fieldset>
                <form method="POST" action="admin/picture_parameter/add" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="label">Label</label>
                        <input type="text" id="label" name="label" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="width">Largeur</label>
                        <input type="number" id="width" name="width" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="height">Hauteur</label>
                        <input type="number" id="height" name="height" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="price">Prix</label>
                        <input type="number" id="price" name="price" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="print">Support d'impression</label>
                        <select name="print" id="print" class="form-control">
                            <?php foreach (PictureParameterRepository::PRINT as $key => $label) : ?>
                                <option value="<?= $key ?>"><?= $label ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <input class="btn btn-success" type="submit" value="Ajouter">
                    </div>
                </form>
            </fieldset>

        </div>
        <div class="col-xl-4 col-lg-6 col-md-8">
            <div class="container">
                <h2>Paramètres photos</h2>
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped" style="font-size: 0.7em;">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Support</th>
                                <th>Label</th>
                                <th>Largeur</th>
                                <th>Hauteur</th>
                                <th>Prix</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($imageParameters as $param) : ?>

                                <tr>
                                    <td class="tdAdmin"><?= $param['id'] ?></td>
                                    <td class="tdAdmin"><?= PictureParameterRepository::getPrintLabel($param['print']) ?></td>
                                    <td class="tdAdmin"><?= $param['label'] ?></td>
                                    <td class="tdAdmin"><?= $param['width'] ?>cm</td>
                                    <td class="tdAdmin"><?= $param['height'] ?>cm</td>
                                    <td class="tdAdmin"><?= $param['price'] ?>€</td>
                                    <td class="tdAdmin">
                                        <a style="color: white;" class="btn btn-info btn-sm" onclick="showUpdateParameterModal(<?= $param['id'] ?>)"> M </a>
                                        <a style="color: white;" class="btn btn-danger btn-sm" onclick="if(confirm('sûr ?')){deletePicParameter(<?= $param['id'] ?>)}"> X </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <div class="col-xl-6 col-lg-12">
            <div class="container">
                <h2>Images en ligne</h2>
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped" style="font-size: 0.9em;">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Id</th>
                                <th>Nom</th>
                                <th>Album</th>
                                <th>Localisation</th>
                                <th>Statut</th>
                                <th>Note</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($images as $image) : ?>

                                <tr>
                                    <td class="tdAdmin">
                                        <a class="thumbnail" title="">
                                            <img class="" style="max-width: 100px; max-height: 50%;" src="assets/upload/<?= $image['nom_photo_min'] ?>" alt="Nature">
                                        </a>
                                    </td>
                                    <td class="tdAdmin"><?= $image['id'] ?></td>
                                    <td class="tdAdmin"><?= $image['nom'] ?></td>
                                    <td class="tdAdmin"><?= $image['label'] ?></td>
                                    <td class="tdAdmin"><?= $image['localisation'] ?></td>
                                    <td class="tdAdmin">
                                        <?php if ($image['has_items'] != 0) : ?>
                                            <span class="badge badge-pill badge-success"><?= $image['has_items'] ?> formats enregistrés</span>
                                        <?php else : ?>
                                            <span class="badge badge-pill badge-danger">Aucun paramètre enregistré</span>
                                        <?php endif; ?>
                                    </td>
                                    <td class="tdAdmin"><?= $image['note'] ?></td>
                                    <td class="tdAdmin">
                                        <a style="color: white;" class="btn btn-info btn-sm" onclick="showItemModal(<?= $image['id'] ?>)"> Gérer </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>