<div class="container-fluid">

    <br>

    <div class="col-md-6 mx-auto text-center" id="alert_container">

        <?php

        use App\Repository\PictureParameterRepository;

        if (isset($succesAjout)) : ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert alert-success alert-dismissible fade show"><?= $succesAjout ?></div>
        <?php elseif (isset($erreurAjout)) : ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert alert-danger alert-dismissible fade show"><?= $erreurAjout ?></div>
        <?php endif; ?>
    </div>

    <br>

    <div class="row">
        <div class="col-xl-10 col-lg-12">
                <h2>Commandes</h2>
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped" style="font-size: 0.9em;">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>User id</th>
                                <th>Email</th>
                                <th>Montant</th>
                                <th>Statut</th>
                                <th>Création</th>
                                <th>Dernière modification</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($orders as $order) : ?>

                                <tr>
                                    <td class="tdAdmin"><?= $order['id'] ?></td>
                                    <td class="tdAdmin"><?= $order['user_id'] ?></td>
                                    <td class="tdAdmin"><?= $order['email'] ?></td>
                                    <td class="tdAdmin"><?= $order['amount'] ?>€</td>
                                    <td class="tdAdmin"><?= $order['status_label'] ?></td>
                                    <td class="tdAdmin"><?= $order['created_at'] ?></td>
                                    <td class="tdAdmin"><?= $order['last_updated_at'] ?></td>
                                    <td class="tdAdmin">
                                        <a style="color: white;" class="btn btn-info btn-sm" onclick=""> Gérer </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
        </div>

    </div>

</div>