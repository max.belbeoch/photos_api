<!-- Modal -->
<div id="updateModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="imgName"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="POST" enctype="multipart/form-data">
                    <input type="hidden" id="idImgToUpdate" value="">
                    <div class="form-group">
                        <label for="">Nom</label>
                        <input type="text" id="newNom" name="newNom" class="form-control" value="">
                    </div>
                    <div class="form-group">
                        <label>Album</label>
                        <select name="newAlbumId" id="newAlbumId" class="form-control">
                            <?php foreach ($albums as $album) : ?>
                                <option value="<?= $album['id'] ?>"><?= $album['nom'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Localisation</label>
                        <input type="text" name="newLocalisation" id="newLocalisation" class="form-control" value="">
                    </div>
                    <div class="form-group">
                        <label for="">Note</label>
                        <select name="newNote" name="newNote" id="newNote" class="form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="newDescription">Description</label>
                        <input type="textarea" name="newDescription" id="newDescription">
                    </div>
                    <div class="form-group">
                        <label for="newToSell">Ajouter à la boutique</label>
                        <input type="checkbox" name="newToSell" id="newToSell">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <input class="btn btn-success" type="submit" value="Modifier" onclick="updatePic()">
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">

    <div class="col-md-6 mx-auto text-center" id="alert_container">
        <?php if (isset($succesAjout)) : ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert alert-success alert-dismissible fade show"><?= $succesAjout ?></div>
        <?php elseif (isset($erreurAjout)) : ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert alert-danger alert-dismissible fade show"><?= $erreurAjout ?></div>
        <?php endif; ?>
    </div>

    <div class="row">
        <div class="col-sm-3 bg-white">
            <h3>Ajouter une image</h3>
            <fieldset>
                <form method="POST" action="admin/picture/add" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="">Nom de l'image</label>
                        <input type="text" id="nomImg" name="nom" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Image</label>
                        <input type="file" id="imageFile" name="image" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Album associé</label>
                        <select name="albumId" id="albumId" class="form-control">
                            <?php foreach ($albums as $album) : ?>
                                <option value="<?= $album['id'] ?>"><?= $album['nom'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Localisation</label>
                        <input type="text" name="localisation" id="localisation" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Note</label>
                        <select name="note" id="note" class="form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="textarea" name="description" id="description">
                    </div>
                    <div class="form-group">
                        <label for="to_sell">Ajouter à la boutique</label>
                        <input type="checkbox" name="to_sell" id="to_sell">
                    </div>
                    <div class="form-group">
                        <input class="btn btn-success" type="submit" value="Ajouter">
                    </div>
                </form>
            </fieldset>
        </div>

        <div class="col-sm-9 bg-white">
            <div class="container">
                <h2>Images</h2>
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Id</th>
                                <th>Nom</th>
                                <th>Album</th>
                                <th>Localisation</th>
                                <th>Boutique</th>
                                <th>Note</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($images as $image) : ?>

                                <tr>
                                    <td class="tdAdmin">
                                        <a class="thumbnail" title="">
                                            <img class="" style="max-width: 100px; max-height: 50%;" src="assets/upload/<?= $image['nom_photo_min'] ?>" alt="Nature">
                                        </a>
                                    </td>
                                    <td class="tdAdmin"><?= $image['id'] ?></td>
                                    <td class="tdAdmin"><?= $image['nom'] ?></td>
                                    <td class="tdAdmin"><?= $image['label'] ?></td>
                                    <td class="tdAdmin"><?= $image['localisation'] ?></td>
                                    <td class="tdAdmin">
                                        <?php if ($image['to_sell'] == 1) : ?>
                                            <span class="badge badge-pill badge-success">Oui</span>
                                        <?php else : ?>
                                            <span class="badge badge-pill badge-danger">Non</span>
                                        <?php endif; ?>
                                    </td>
                                    <td class="tdAdmin"><?= $image['note'] ?></td>
                                    <td class="tdAdmin">
                                        <a style="color: white;" class="btn btn-info btn-sm" onclick="showUpdateModal(<?= $image['id'] ?>)"> Modifier </a>
                                        <a style="color: white;" class="btn btn-danger btn-sm" onclick="if(confirm('sûr ?')){deletePic(<?= $image['id'] ?>)}"> X </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>