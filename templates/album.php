<section>

    <div class="container-fluid">
        <div class="row">
            <?php foreach ($images as $key => $image) : ?>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="overlay-image _bp">
                        <a href="album/<?= $albumId ?>#img_light" onclick="displayImg(<?= $image['id'] ?>)">
                            <img src="assets/upload/<?= $image["nom_photo_min"] ?>" style="width: 100%;">
                            <div class="hover _br">
                                <div class="text _q"><?= $image["nom"] ?></div>
                            </div>
                        </a>
                    </div>
                    <a href="album/<?= $albumId ?>#_" class="lightbox" id="img_light">
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div><br>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php if ($params['pageNum'] != null && $params['pageNum'] > 1) : ?>
                    <a class="arrow arrow-left" title="Page précédente" href="<?= $baseUrl . '/' . (string) ($params['pageNum'] - 1)  ?>"></a>
                <?php endif; ?>
                <?php if ($params['pageNum'] < $nbPages) : ?>
                    <a class="arrow arrow-right" title="Page suivante" href="<?= $baseUrl . '/' . (string) ($params['pageNum'] + 1)  ?>"></a>
                <?php endif; ?>
            </div>
        </div>
    </div>

</section>