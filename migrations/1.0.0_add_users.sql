CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` varchar(64) NOT NULL DEFAULT '2021-03-26 22:21:29',
  `last_updated_at` varchar(64) NOT NULL DEFAULT '2021-03-26 22:21:29',
  `deleted_at` varchar(64) NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_id_unique` (`id`);
 
INSERT INTO `user` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `created_at`, `last_updated_at`) VALUES
(2, 'test', 'Max', 'test', 'maxime.belbeoch5@gmail.com', '$2y$10$insG5TxbqDXFlhGX0ovVx.gR96JaqEfcI5FRvCh6UpmaYm0wGgXK2', NOW() , NOW());
 
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;