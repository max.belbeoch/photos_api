ALTER TABLE `image` DROP `date`;

ALTER TABLE `image` ADD `description` VARCHAR(500) NULL AFTER `to_sell`, 
ADD `created_at` DATETIME NULL AFTER `description`, 
ADD `last_updated_at` DATETIME NULL AFTER `created_at`, 
ADD `deleted_at` DATETIME NULL AFTER `last_updated_at`;

