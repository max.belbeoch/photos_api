ALTER TABLE `user` ADD `is_admin` TINYINT NOT NULL DEFAULT '0' AFTER `password`, 
ADD `token` VARCHAR(255) NULL AFTER `is_admin`, 
ADD `token_expiration_date` DATETIME NULL AFTER `token`;
ADD `last_login` DATETIME NULL AFTER `token_expiration_date`;