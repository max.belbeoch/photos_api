<?php

namespace App\Action;

use App\Repository\PictureRepository;
use App\Service\PictureService;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AlbumAction
{

    private $pictureService;
    private $renderer;
    private $pictureRepository;

    public function __construct(PictureService $pictureService, PictureRepository $pictureRepository, PhpRenderer $renderer)
    {
        $this->pictureService = $pictureService;
        $this->renderer = $renderer;
        $this->pictureRepository = $pictureRepository;

        $this->renderer->setLayout('layout.php');
    }

    public function __invoke(Request $request,  Response $response, $args): Response
    {
        
        if (!$this->pictureRepository->albumExists($args['id'])) {
            return $this->sendError($response, 'Aucun album trouvé');
        }

        $albumId = $args['id'];

		$baseUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/photos_api/album/' . $albumId;

		$nbPages = $this->pictureRepository->getAlbumNbImg($albumId);

		$params['pageNum'] = (!empty($args['pageNum']) && ($args['pageNum'] <= (int) $nbPages + 1)) ? (int) $args['pageNum'] : 1;

		$images = $this->pictureRepository->getAlbumImg($albumId, $params);

        $res = [
            'baseUrl' => $baseUrl,
            'images' => $images,
            'params' => $params,
            'nbPages' => $nbPages,
            'page' => 'album',
            'albumId' => $albumId
        ];

        return $this->renderer->render($response, 'album.php', $res);
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        return $this->renderer->render($response, '404.php', ['message' => $error])
            ->withStatus(404);
    }
}
