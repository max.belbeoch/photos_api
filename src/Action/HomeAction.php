<?php

namespace App\Action;

use Slim\Views\PhpRenderer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class HomeAction
{

    private $renderer;

    public function __construct(PhpRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    public function __invoke(Request $request,  Response $response): Response
    {
        $this->renderer->setLayout('layout.php' );

        return $this->renderer->render($response, 'home.php', ['route' => 'home', 'page' => 'home']);
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        return $this->renderer->render($response, '404.php', ['message' => $error])
            ->withStatus(400);
    }
}
