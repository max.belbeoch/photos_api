<?php

namespace App\Action;

use Psr\Container\ContainerInterface;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Service\MailerService;

final class ContactAction
{
    private $renderer;
    private $container;
    private $mailerService;

    public function __construct(PhpRenderer $renderer, ContainerInterface $container, MailerService $mailer)
    {
        $this->container = $container;
        $this->mailerService = $mailer;
        $this->renderer = $renderer;
        $this->renderer->setLayout('layout.php');
    }

    public function __invoke(Request $request,  Response $response): Response
    {

        if (isset($_POST) && !empty($_POST)) {

            // (B) VERIFY CAPTCHA
            $secret = $this->container->get('settings')['captcha_secret'];
            $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=" . $_POST['g-recaptcha-response'];
            $verify = json_decode(file_get_contents($url));

            if (!$verify->success) {

                $result = [
                    'result' => false,
                    'message' => 'Une erreur est survenue',
                ];
        
                $response->getBody()->write((string)json_encode($result));
        
                return $response
                    ->withHeader('Content-Type', 'application/json')
                    ->withStatus(400);
            } else {

                $post = (array)$request->getParsedBody();

                $datas['from'] = 'contact@maxime-belbeoch.com';
                $datas['to'] = htmlspecialchars($post['contact_mail']);
                $datas['subject'] = 'Contact portfolio';
                $datas['template'] = 'emails/contact.html.twig';
                $datas['content'] = [
                    'contact_name' => htmlspecialchars($post['contact_name']),
                    'contact_mail' => htmlspecialchars($post['contact_mail']),
                    'contact_subject' => htmlspecialchars($post['contact_subject']),
                    'contact_message' => htmlspecialchars($post['contact_message']),
                ];

                $this->mailerService->sendEmail($datas);

                $result = [
                    'result' => true,
                    'message' => 'Message envoyé avec succès !',
                ];
        
                $response->getBody()->write((string)json_encode($result));
        
                return $response
                    ->withHeader('Content-Type', 'application/json')
                    ->withStatus(200);
            }
        }

        return $this->renderer->render($response, 'contact.php', ['page' => 'contact']);
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        return $this->renderer->render($response, '404.php', ['message' => $error])
            ->withStatus(400);
    }
}
