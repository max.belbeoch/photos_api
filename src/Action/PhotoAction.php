<?php

namespace App\Action;

use App\Service\PictureService;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class PhotoAction
{

    private $renderer;
    private $pictureService;

    public function __construct(PhpRenderer $renderer, PictureService $pictureService)
    {
        $this->renderer = $renderer;
        $this->pictureService = $pictureService;
    }

    public function __invoke(Request $request,  Response $response, $args)
    {
        $this->pictureService->getImgResized($args);
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        return $this->renderer->render($response, '404.php', ['message' => $error])
            ->withStatus(400);
    }
}
