<?php

namespace App\Action;

use App\Repository\PictureRepository;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AlbumsAction
{
    private $pictureRepository;
    private $renderer;

    public function __construct(PictureRepository $pictureRepository, PhpRenderer $renderer)
    {
        $this->pictureRepository = $pictureRepository;
        $this->renderer = $renderer;

        $this->renderer->setLayout('layout.php');
    }

    public function __invoke(Request $request,  Response $response): Response
    {
        $albums = $this->pictureRepository->getAllAlbums();

        if (!$albums) {
            return $this->sendError($response, 'Aucun album trouvé');
        }

        return $this->renderer->render($response, 'albums.php', ['albums' => $albums, 'page' => 'albums']);
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        return $this->renderer->render($response, '404.php', ['message' => $error])
            ->withStatus(400);
    }
}
