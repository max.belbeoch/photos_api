<?php

namespace App\Action\User;

use App\Repository\UserRepository;
use App\Service\UserService;
use Odan\Session\SessionInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class UserLoginAction
{

    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function __invoke(Request $request,  Response $response): Response
    {

        $data = (array) $request->getQueryParams();

        if(empty($data)){
            $data = $request->getParsedBody();
        }

        $token = $this->userService->loginUser($data);

        if (!$token) {
            return $this->sendError($response, 'Identifiants invalides');
        }

        $result = [
            'result' => true,
            'token' => $token,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }


    private function sendError($response, $error)
    {
        $result = [
            'result' => false,
            'error' => $error,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}
