<?php

namespace App\Action\User;

use App\Repository\UserRepository;
use App\Service\UserService;
use Odan\Session\SessionInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ServerRequestInterface;

final class IsLoggedAction
{

    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function __invoke(ServerRequestInterface $request,  Response $response): Response
    {

        $logged = $this->userService->isLogged($request);

        if (!$logged) {
            return $this->sendError($response, 'not connected');
        }

        $result = [
            'result' => true
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }


    private function sendError($response, $error)
    {
        $result = [
            'result' => false,
            'error' => $error,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}
