<?php

namespace App\Action\User;

use App\Repository\UserRepository;
use App\Service\UserService;
use App\Service\MailerService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class UserRegisterAction
{
    private $userService;
    private $mailerService;
    private $repository;

    public function __construct(UserService $userService, UserRepository $repository, MailerService $mailerService)
    {
        $this->userService = $userService;
        $this->mailerService = $mailerService;
        $this->repository = $repository;
    }

    public function __invoke(Request $request,  Response $response): Response
    {
        $data = (array)$request->getParsedBody();

        $user = [
            'first_name' => htmlspecialchars($data['first_name']),
            'last_name' => htmlspecialchars($data['last_name']),
            'email' => $data['email'],
            'phone_number' => htmlspecialchars($data['phone_number']),
            'password' => $data['password'],
        ];

        $mailExists = $this->repository->getIdByMail($user['email']);

        if($mailExists){
            return $this->sendError($response, 'Mail déjà utilisé');
        }

        $userId = $this->userService->createUser($user);

        $user = $this->repository->getById($userId);

        $this->mailerService->sendSignupMail($user);

        $result = [
            'result' => true,
            'user_id' => $userId,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(201);
    }

    private function sendError($response, $error)
    {
        $result = [
            'result' => false,
            'error' => $error,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}
