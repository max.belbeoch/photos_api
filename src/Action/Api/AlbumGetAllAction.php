<?php

namespace App\Action\Api;

use App\Service\PictureService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AlbumGetAllAction
{

    private $pictureService;

    public function __construct(PictureService $pictureService)
    {
        $this->pictureService = $pictureService;
    }

    public function __invoke(Request $request,  Response $response): Response
    {
        
        $albums = $this->pictureService->getAllAlbumToSell($request);

        if (!$albums) {
            return $this->sendError($response, 'no albums found');
        }

        $result = [
            'result' => true,
            'albums' => $albums,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }


    private function sendError($response, $error)
    {
        $result = [
            'result' => false,
            'error' => $error,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
