<?php

namespace App\Action\Api;

use App\Repository\PictureItemRepository;
use App\Repository\PictureParameterRepository;
use App\Service\PictureService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class PictureGetOneAction
{

    private $pictureService;
    private $pictureItemRepository;
    private $pictureParameterRepository;

    public function __construct(PictureService $pictureService, PictureItemRepository $pictureItemRepository, PictureParameterRepository $pictureParameterRepository)
    {
        $this->pictureService = $pictureService;
        $this->pictureItemRepository = $pictureItemRepository;
        $this->pictureParameterRepository = $pictureParameterRepository;
    }

    public function __invoke(Request $request,  Response $response, $args): Response
    {

        if (empty($args)) {
            return $this->sendError($response, 'invalid argument');
        }

        $pic = $this->pictureService->getPictureToSell($request, $args['id']);

        if (!$pic) {
            return $this->sendError($response, 'picture not found');
        }

        $items = $this->pictureItemRepository->getPictureItems($args['id']) ?: false;
        $pic->parameters = false;

        if ($items) {
            $pic->parameters = [];

            foreach ($items as &$item) {
                $parameter = $this->pictureParameterRepository->getPictureParameter($item['image_parameter_id']);
                $item['label'] = PictureParameterRepository::getPrintLabel($parameter->print);
                $item['format'] = $parameter;
                $pic->parameters[$parameter->print]['parameter_print_id'] = $parameter->print;
                $pic->parameters[$parameter->print]['label'] = $item['label'];
                $pic->parameters[$parameter->print]['data'][] = $item;
            }
        }

        $result = [
            'result' => true,
            'picture' => $pic,
        ];

        $response->getBody()->write((string)json_encode($result, true));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }


    private function sendError($response, $error)
    {
        $result = [
            'result' => false,
            'error' => $error,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
