<?php

namespace App\Action\Api;

use App\Service\PictureService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AlbumGetOneAction
{

    private $pictureService;

    public function __construct(PictureService $pictureService)
    {
        $this->pictureService = $pictureService;
    }

    public function __invoke(Request $request,  Response $response, $args): Response
    {

        if (empty($args)) {
            return $this->sendError($response, 'invalid argument');
        }

       $album = $this->pictureService->getAlbumToSell($request, $args['id']);
    
        if (!$album) {
            return $this->sendError($response, 'picture not found');
        }

        $result = [
            'result' => true,
            'album' => $album,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }


    private function sendError($response, $error)
    {
        $result = [
            'result' => false,
            'error' => $error,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
