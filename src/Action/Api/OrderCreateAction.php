<?php

namespace App\Action\Api;

use App\Repository\OrderItemRepository;
use App\Repository\PictureItemRepository;
use App\Repository\PictureParameterRepository;
use App\Repository\UserOrderRepository;
use App\Service\OrderService;
use App\Service\UserService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class OrderCreateAction
{

    private $orderItemRepository;
    private $userOrderRepository;
    private $pictureItemRepository;
    private $pictureParameterRepository;
    private $userService;

    public function __construct(OrderItemRepository $orderItemRepository, UserOrderRepository $userOrderRepository, PictureItemRepository $pictureItemRepository, PictureParameterRepository $pictureParameterRepository, UserService $userService)
    {
        $this->orderItemRepository = $orderItemRepository;
        $this->userOrderRepository = $userOrderRepository;
        $this->pictureItemRepository = $pictureItemRepository;
        $this->pictureParameterRepository = $pictureParameterRepository;
        $this->userService = $userService;
    }

    public function __invoke(Request $request,  Response $response, $args): Response
    {

        $datas = $request->getParsedBody();

        if (isset($datas) && !empty($datas)) {

            $user = $this->userService->getUserByToken($request);

            $orderDatas = [
                'status' => UserOrderRepository::ORDER_CREATED,
                'amount' => null,
                'user_id' => $user->id
            ];

            $orderId = $this->userOrderRepository->insertUserOrder($orderDatas);
            $amountOrderTotal = 0;

            foreach ($datas as $data) {

                $picItem = $this->pictureItemRepository->getPictureItem($data['id']);
                $picParameter = $this->pictureParameterRepository->getPictureParameter($picItem->image_parameter_id);

                if ($picItem && !empty($picItem) && $picParameter && !empty($picParameter)) {

                    $item = [
                        'image_item_id' => $picItem->id,
                        'order_id' => $orderId,
                        'quantity' => $data['quantity']
                    ];

                    $amountOrderTotal += $picParameter->price * $data['quantity'] ?: 1;

                    $this->orderItemRepository->insertOrderItem($item);
                } else {
                    return $this->sendError($response, 'item inconnu');
                }
            }
            $this->userOrderRepository->setAmount(['id' => $orderId, 'amount' => $amountOrderTotal]);
        }

        $result = [
            'result' => true
        ];

        $response->getBody()->write((string)json_encode($result, true));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }


    private function sendError($response, $error)
    {
        $result = [
            'result' => false,
            'error' => $error,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
