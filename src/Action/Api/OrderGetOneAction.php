<?php

namespace App\Action\Api;

use App\Repository\OrderItemRepository;
use App\Repository\UserOrderRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class OrderGetOneAction
{

    private $userOrderRepository;
    private $orderItemRepository;

    public function __construct(UserOrderRepository $userOrderRepository, OrderItemRepository $orderItemRepository)
    {
        $this->userOrderRepository = $userOrderRepository;
        $this->orderItemRepository = $orderItemRepository;
    }

    public function __invoke(Request $request,  Response $response): Response
    {

        if (empty($args)) {
            return $this->sendError($response, 'invalid argument');
        }

       $order = $this->userOrderRepository->getUserOrder($args['id']);
    
        if (!$order) {
            return $this->sendError($response, 'picture not found');
        }

        $result = [
            'result' => true,
            'order' => $order,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }


    private function sendError($response, $error)
    {
        $result = [
            'result' => false,
            'error' => $error,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
