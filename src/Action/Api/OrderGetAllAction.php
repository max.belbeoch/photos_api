<?php

namespace App\Action\Api;

use App\Repository\OrderItemRepository;
use App\Repository\UserOrderRepository;
use App\Service\UserService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class OrderGetAllAction
{
    private $userOrderRepository;
    private $userService;

    public function __construct(UserOrderRepository $userOrderRepository, UserService $userService)
    {
        $this->userOrderRepository = $userOrderRepository;
        $this->userService = $userService;
    }

    public function __invoke(Request $request,  Response $response): Response
    {

        $user = $this->userService->getUserByToken($request);
        
        $orders = $this->userOrderRepository->getAllOrderByUserId($user->id);

        if (!$orders) {
            return $this->sendError($response, 'no orders found');
        }

        $result = [
            'result' => true,
            'orders' => $orders,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }


    private function sendError($response, $error)
    {
        $result = [
            'result' => false,
            'error' => $error,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
