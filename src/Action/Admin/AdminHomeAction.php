<?php

namespace App\Action\Admin;

use App\Repository\PictureRepository;
use Slim\Views\PhpRenderer;
use Odan\Session\SessionInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AdminHomeAction
{
    private $pictureRepository;
    private $renderer;
    private $session;

    public function __construct(PhpRenderer $renderer, SessionInterface $session, PictureRepository $pictureRepository)
    {
        $this->renderer = $renderer;
        $this->pictureRepository = $pictureRepository;
        $this->session = $session;
    }

    public function __invoke(Request $request,  Response $response): Response
    {
        $this->renderer->setLayout('admin/layout.php' );

        $pictures = $this->pictureRepository->getAllPictures();
        $albums = $this->pictureRepository->getAllAlbums();

        foreach( $pictures as &$pic){
            $pic['label'] = $this->pictureRepository->getAlbumName($pic['album_id']);
        }

        return $this->renderer->render($response, 'admin/home.php', [
            'route' => 'admin_home',
            'images' => $pictures,
            'albums' => $albums
            ]
        );
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        return $this->renderer->render($response, '404.php', ['message' => $error])
            ->withStatus(400);
    }
}
