<?php

namespace App\Action\Admin;

use App\Repository\OrderItemRepository;
use App\Repository\UserOrderRepository;
use App\Repository\UserRepository;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AdminOrderAction
{
    private $userOrderRepository;
    private $orderItemRepository;
    private $userRepository;
    private $renderer;

    public function __construct(PhpRenderer $renderer, UserOrderRepository $userOrderRepository, OrderItemRepository $orderItemRepository, UserRepository $userRepository)
    {
        $this->renderer = $renderer;
        $this->userOrderRepository = $userOrderRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->userRepository = $userRepository;
    }

    public function __invoke(Request $request,  Response $response): Response
    {
        $this->renderer->setLayout('admin/layout.php');

        $orders = $this->userOrderRepository->getAllUserOrder();

        foreach ($orders as $order) {

            $user = $this->userRepository->getById($order['user_id']);

            $datas[] = [
                'id' => $order['id'],
                'user_id' => $user->id,
                'email' => $user->email,
                'amount' => $order['amount'],
                'status' => $order['status'],
                'status_label' => UserOrderRepository::getOrderStatusLabelPill($order['status']),
                'created_at' => $order['created_at'],
                'last_updated_at' => $order['last_updated_at'],
                'items' => []
            ];
        }

        return $this->renderer->render(
            $response,
            'admin/order.php',
            [
                'route' => 'admin_order',
                'orders' => $datas
            ]
        );
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        return $this->renderer->render($response, '404.php', ['message' => $error])
            ->withStatus(400);
    }
}
