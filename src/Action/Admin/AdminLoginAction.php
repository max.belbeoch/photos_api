<?php

namespace App\Action\Admin;

use App\Repository\UserRepository;
use App\Service\UserService;
use Odan\Session\SessionInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\PhpRenderer;

final class AdminLoginAction
{
    private $renderer;
    private $userService;
    private $session;
    private $repository;

    public function __construct(UserService $userService, PhpRenderer $renderer, SessionInterface $session, UserRepository $repository)
    {
        $this->userService = $userService;
        $this->renderer = $renderer;
        $this->session = $session;
        $this->repository = $repository;
    }

    public function __invoke(Request $request,  Response $response): Response
    {

        $this->renderer->setLayout('admin/layout.php');

        //print_r($request->getAttribute('decoded_token_data'));

        if (isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['password']) && !empty($_POST['password'])) {

            // Clear all flash messages
            $flash = $this->session->getFlash();
            $flash->clear();

            $token = $this->userService->loginUser($_POST, true);

            if (!$token) {
                $flash->add('error', 'Invalid credentials');
                return $response->withStatus(200)->withHeader('Location', 'login');
            }

            $user = $this->repository->getUser($_POST, true);

            $this->session->destroy();
            $this->session->start();
            $this->session->regenerateId();

            $this->session->set('user', $user);
            $flash->add('success', 'Login successfully');

            return $response->withStatus(200)->withHeader('Location', 'home');
        }

        return $this->renderer->render(
            $response,
            'admin/login.php',
            [
                'route' => 'admin_login',
            ]
        );
    }


    private function sendError($response, $error)
    {
        $result = [
            'result' => false,
            'error' => $error,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
