<?php

namespace App\Action\Admin\Picture;

use App\Repository\PictureRepository;
use Psr\Container\ContainerInterface;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AdminDeletePictureAction
{
    private $pictureRepository;
    private $container;

    public function __construct(PictureRepository $pictureRepository, ContainerInterface $container)
    {
        $this->pictureRepository = $pictureRepository;
        $this->container = $container;
    }

    public function __invoke(Request $request,  Response $response): Response
    {
        $picture = $this->pictureRepository->getPicture($_POST['id'], true);

        // move files to old repo
        $settings = $this->container->get('settings');
        rename($settings['public'] . '/assets/upload/' . $picture->nom_photo, $settings['public'] . '/assets/upload/old/' . $picture->nom_photo);
        rename($settings['public'] . '/assets/upload/' . $picture->nom_photo_min, $settings['public'] . '/assets/upload/old/' . $picture->nom_photo_min);

        // check admin before
        $this->pictureRepository->deletePicture($_POST['id']);

        $response->getBody()->write((string)json_encode(['result' => true]));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        $response->getBody()->write((string)json_encode($error));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
