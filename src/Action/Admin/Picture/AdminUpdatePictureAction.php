<?php

namespace App\Action\Admin\Picture;

use App\Repository\PictureRepository;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AdminUpdatePictureAction
{
    private $pictureService;

    public function __construct(PictureRepository $pictureRepository)
    {
        $this->pictureRepository = $pictureRepository;
    }

    public function __invoke(Request $request,  Response $response): Response
    {

        if (isset($_POST['id']) && !empty($_POST['id'])) {

            $pic = [
                'nom' => htmlspecialchars($_POST['newNom']),
                'album_id' => htmlspecialchars($_POST['newAlbumId']),
                'localisation' => htmlspecialchars($_POST['newLocalisation']),
                'note' => htmlspecialchars($_POST['newNote']),
                'to_sell' => isset($_POST['newToSell']) && !empty($_POST['newToSell']) ? 1 : 0,
                'description' => htmlspecialchars($_POST['newDescription'])
            ];

            $res = $this->pictureRepository->updatePicture($pic, $_POST['id']);
        }

        $response->getBody()->write((string)json_encode(['result' => true]));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        $response->getBody()->write((string)json_encode($error));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
