<?php

namespace App\Action\Admin\Picture;

use App\Repository\PictureRepository;
use App\Service\PictureResizeService;
use Psr\Container\ContainerInterface;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AdminAddPictureAction
{
    private $pictureRepository;
    private $container;
    private $renderer;

    public function __construct(PictureRepository $pictureRepository, ContainerInterface $container, PhpRenderer $renderer)
    {
        $this->pictureRepository = $pictureRepository;
        $this->container = $container;
        $this->renderer = $renderer;
    }

    public function __invoke(Request $request,  Response $response): Response
    {

        if (isset($_POST['nom'], $_FILES['image'])) {
            if (!empty($_POST['nom']) && !empty($_FILES['image'])) {
                $settings = $this->container->get('settings');
                $indiceImage = $this->pictureRepository->getLastPicId() + 1;

                $pathUpload = $settings['public'] . '/assets/upload/';
                $filename = htmlspecialchars($_FILES['image']['name']);

                $pic = [
                    'nom' => htmlspecialchars($_POST['nom']),
                    'nom_photo' => htmlspecialchars($indiceImage) . '_' . $filename,
                    'nom_photo_min' => htmlspecialchars($indiceImage) . '_min_' . $filename,
                    'album_id' => htmlspecialchars($_POST['albumId']),
                    'localisation' => htmlspecialchars($_POST['localisation']),
                    'note' => htmlspecialchars($_POST['note']),
                    'to_sell' => isset($_POST['to_sell']) && !empty($_POST['to_sell']) ? 1 : 0,
                    'description' => htmlspecialchars($_POST['description'])
                ];

                $this->pictureRepository->insertPicture($pic);

                move_uploaded_file($_FILES['image']['tmp_name'], $pathUpload . $pic['nom_photo']);

                $resizeObj = new PictureResizeService($pathUpload . $pic['nom_photo']);
                $resizeObj->resizeImage(500, 500, 'crop');
                $resizeObj->saveImage($pathUpload . $pic['nom_photo_min'], 500);

                return $response->withStatus(200)->withHeader('Location', '../home');
            }
        }

        return $this->sendError($response);
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        $response->getBody()->write((string)json_encode($error));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
