<?php

namespace App\Action\Admin;

use App\Repository\PictureParameterRepository;
use App\Repository\PictureItemRepository;
use App\Repository\PictureRepository;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AdminShopAction
{
    private $pictureRepository;
    private $pictureItemRepository;
    private $pictureParameterRepository;
    private $renderer;

    public function __construct(PhpRenderer $renderer, PictureRepository $pictureRepository, PictureParameterRepository $pictureParameterRepository, PictureItemRepository $pictureItemRepository)
    {
        $this->renderer = $renderer;
        $this->pictureRepository = $pictureRepository;
        $this->pictureParameterRepository = $pictureParameterRepository;
        $this->pictureItemRepository = $pictureItemRepository;
    }

    public function __invoke(Request $request,  Response $response): Response
    {
        $this->renderer->setLayout('admin/layout.php' );

        $pictures = $this->pictureRepository->getAllPicturesToSell();
        $albums = $this->pictureRepository->getAllAlbums();

        foreach( $pictures as &$pic){
            $pic['has_items'] = $this->pictureItemRepository->hasItem($pic['id']);
            $pic['label'] = $this->pictureRepository->getAlbumName($pic['album_id']);
        }

        $imageParameters = $this->pictureParameterRepository->getAllPictureParameter();

        return $this->renderer->render($response, 'admin/shop.php', [
            'route' => 'admin_shop',
            'imageParameters' => $imageParameters,
            'images' => $pictures,
            'albums' => $albums
            ]
        );
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        return $this->renderer->render($response, '404.php', ['message' => $error])
            ->withStatus(400);
    }
}
