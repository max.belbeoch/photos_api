<?php

namespace App\Action\Admin\PictureParameter;

use App\Repository\PictureParameterRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AdminDeletePictureParameterAction
{
    private $pictureParameterRepository;

    public function __construct(PictureParameterRepository $pictureParameterRepository)
    {
        $this->pictureParameterRepository = $pictureParameterRepository;
    }

    public function __invoke(Request $request,  Response $response): Response
    {
        // check admin before
        $this->pictureParameterRepository->DeletePictureParameter($_POST['id']);

        $response->getBody()->write((string)json_encode(['result' => true]));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        $response->getBody()->write((string)json_encode($error));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
