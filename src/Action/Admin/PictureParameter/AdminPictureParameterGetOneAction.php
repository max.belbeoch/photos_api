<?php

namespace App\Action\Admin\PictureParameter;

use App\Repository\PictureParameterRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AdminPictureParameterGetOneAction
{

    private $pictureParameterRepository;

    public function __construct(PictureParameterRepository $pictureParameterRepository)
    {
        $this->pictureParameterRepository = $pictureParameterRepository;
    }

    public function __invoke(Request $request,  Response $response, $args): Response
    {

        if (empty($args)) {
            return $this->sendError($response, 'invalid argument');
        }

       $param = $this->pictureParameterRepository->getPictureParameter($args['id']);
    
        if (!$param) {
            return $this->sendError($response, 'parameter not found');
        }

        $result = [
            'result' => true,
            'parameter' => $param,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }


    private function sendError($response, $error)
    {
        $result = [
            'result' => false,
            'error' => $error,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
