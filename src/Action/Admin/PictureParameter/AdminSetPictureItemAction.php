<?php

namespace App\Action\Admin\PictureParameter;

use App\Repository\PictureParameterRepository;
use App\Repository\PictureItemRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AdminSetPictureItemAction
{
    private $pictureParameterRepository;
    private $pictureItemRepository;

    public function __construct(PictureParameterRepository $pictureParameterRepository, PictureItemRepository $pictureItemRepository)
    {
        $this->pictureParameterRepository = $pictureParameterRepository;
        $this->pictureItemRepository = $pictureItemRepository;
    }

    public function __invoke(Request $request,  Response $response): Response
    {

        if (isset($_POST['id']) && !empty($_POST['id'])) {

            foreach($_POST['data'] as $param){

                $args = [
                    'image_id' => $_POST['id'],
                    'image_parameter_id' => $param['parameter_id']                
                ];

                $item = $this->pictureItemRepository->getPictureItemByParamId($args['image_id'], $args['image_parameter_id']);

                if(!$item && $param['checked'] == 1){
                    $this->pictureItemRepository->insertPictureItem($args);
                } else if ($item && $param['checked'] == 0){
                    $this->pictureItemRepository->DeletePictureItem($item->id);
                }
            }
        }

        $response->getBody()->write((string)json_encode(['result' => true]));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        $response->getBody()->write((string)json_encode($error));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
