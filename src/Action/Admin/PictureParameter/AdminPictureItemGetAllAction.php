<?php

namespace App\Action\Admin\PictureParameter;

use App\Repository\PictureParameterRepository;
use App\Repository\PictureItemRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AdminPictureItemGetAllAction
{
    
    private $pictureItemRepository;
    private $pictureParameterRepository;

    public function __construct(PictureItemRepository $pictureItemRepository, PictureParameterRepository $pictureParameterRepository)
    {
        $this->pictureParameterRepository = $pictureParameterRepository;
        $this->pictureItemRepository = $pictureItemRepository;
    }

    public function __invoke(Request $request,  Response $response, $args): Response
    {

        if (empty($args)) {
            return $this->sendError($response, 'invalid argument');
        }

        $params = $this->pictureParameterRepository->getAllPictureParameter();

        foreach ($params as &$param) {

            $searchArgs = [
                'image_id' => $args['id'],
                'image_parameter_id' => $param['id']                
            ];

            $item = $this->pictureItemRepository->getPictureItemByParamId($searchArgs['image_id'], $searchArgs['image_parameter_id']);
            $item ? $param['checked'] = 1 : $param['checked'] = 0;
            $param['print'] = PictureParameterRepository::getPrintLabel($param['print']);
        }

        $result = [
            'result' => true,
            'parameters' => $params,
        ];

        $response->getBody()->write((string) json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }


    private function sendError($response, $error)
    {
        $result = [
            'result' => false,
            'error' => $error,
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
