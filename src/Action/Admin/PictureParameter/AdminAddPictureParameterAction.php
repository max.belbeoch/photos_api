<?php

namespace App\Action\Admin\PictureParameter;

use App\Repository\PictureParameterRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AdminAddPictureParameterAction
{
    private $pictureParameterRepository;

    public function __construct(PictureParameterRepository $pictureParameterRepository)
    {
        $this->pictureParameterRepository = $pictureParameterRepository;
    }

    public function __invoke(Request $request,  Response $response): Response
    {

        if (isset($_POST['label']) && !empty($_POST['label'])) {

            $parameter = [
                'label' => htmlspecialchars($_POST['label']),
                'print' => htmlspecialchars($_POST['print']),
                'width' => htmlspecialchars($_POST['width']),
                'height' => htmlspecialchars($_POST['height']),
                'price' => htmlspecialchars($_POST['price']) // convert centimes
            ];

            $this->pictureParameterRepository->insertPictureParameter($parameter);

            return $response->withStatus(200)->withHeader('Location', '../shop');
        }

        return $this->sendError($response);
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        $response->getBody()->write((string)json_encode($error));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(400);
    }
}
