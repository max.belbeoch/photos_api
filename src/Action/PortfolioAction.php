<?php

namespace App\Action;

use App\Repository\PictureRepository;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class PortfolioAction
{

    private $pictureService;
    private $renderer;
    private $pictureRepository;

    public function __construct(PictureRepository $pictureRepository, PhpRenderer $renderer)
    {
        $this->pictureRepository = $pictureRepository;
        $this->renderer = $renderer;

        $this->renderer->setLayout('layout.php');
    }

    public function __invoke(Request $request,  Response $response): Response
    {
        $pics = $this->pictureRepository->getPortfolioContent();

        if (!$pics) {
            return $this->sendError($response, 'Aucune image trouvée');
        }

        return $this->renderer->render($response, 'portfolio.php', ['images' => $pics, 'page' => 'portfolio']);
    }

    private function sendError($response, $error = 'Une erreur est survenue')
    {
        return $this->renderer->render($response, '404.php', ['message' => $error])
            ->withStatus(400);
    }
}
