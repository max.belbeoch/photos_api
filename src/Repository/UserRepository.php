<?php

namespace App\Repository;

use PDO;

/**
 * Repository.
 */
class UserRepository
{
    /**
     * @var PDO The database connection
     */
    private $db;

    const TOKEN_VALIDITY_MINUTES = 30;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * Get user row.
     *
     *
     * @return User The pic
     */
    public function getById($id)
    {
        $sql = "SELECT * FROM user WHERE id=:id AND `deleted_at` IS NULL ";

        $sth = $this->db->prepare($sql);
        $sth->bindParam("id", $id);
        $sth->execute();
        $user = $sth->fetchObject();

        return $user;
    }

    /**
     * Get user row.
     *
     *
     * @return User The pic
     */
    public function getIdByMail($mail)
    {
        $sql = "SELECT id FROM user WHERE email=:email AND `deleted_at` IS NULL ";

        $sth = $this->db->prepare($sql);
        $sth->bindParam("email", $mail);
        $sth->execute();
        $user = $sth->fetchObject();

        return $user;
    }

    /**
     * Insert user row.
     *
     * @param array $user The user
     *
     * @return int The new ID
     */
    public function insertUser(array $row): int
    {
        $row['password'] = password_hash($row['password'], PASSWORD_DEFAULT);

        $sql = "INSERT INTO user SET 
                first_name=:first_name, 
                last_name=:last_name, 
                phone_number=:phone_number,
                email=:email,
                password=:password,
                created_at=NOW(),
                last_updated_at=NOW();";

        $this->db->prepare($sql)->execute($row);

        return (int)$this->db->lastInsertId();
    }

    /**
     * Get user row.
     *
     * @param array $id The user id
     *
     * @return User The user
     */
    public function getUser(array $user, $min = false, $isAdmin = false)
    {
        $row = [
            'email' => $user['email'],
        ];

        if($min){
            $sql = "SELECT id, first_name, last_name, email, is_admin, token  FROM user WHERE email=:email ";
        } else {
            $sql = "SELECT * FROM user WHERE email=:email ";
        }

        if($isAdmin){
            $sql .= " AND is_admin = 1";
        }

        $sth = $this->db->prepare($sql);
        $sth->bindParam("email", $row['email']);
        $sth->execute();
        $res = $sth->fetchObject();

        return $res;
    }

    /**
     * Set user token.
     *
     * @param Array $args
     *
     * @return User The user
     */
    public function setUserToken(array $args)
    {
        $minutes_to_add = self::TOKEN_VALIDITY_MINUTES;
        $expiration = date('Y-m-d H:i:s', strtotime("+{$minutes_to_add} minutes"));

        $row = [
            'token' => $args['token'],
            'expiration'=> $expiration,
            'id' => $args['id']
        ];
        
        $sql = "UPDATE user SET 
                token=:token, 
                last_login=NOW(),
                token_expiration_date=:expiration
                WHERE id=:id";

        return $this->db->prepare($sql)->execute($row);
    }
}
