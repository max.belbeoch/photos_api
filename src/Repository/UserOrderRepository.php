<?php

namespace App\Repository;

use PDO;

/**
 * Repository.
 */
class UserOrderRepository
{
    /**
     * @var PDO The database connection
     */
    private $db;

    const ORDER_CREATED = 1;
    const ORDER_PENDING = 2;
    const ORDER_CANCELED = 3;
    const ORDER_VALIDATED = 4;
    const ORDER_SENT = 5;
    const ORDER_REPAID = 6;

    const ORDER_STATUS_LABEL = [
        self::ORDER_CREATED => 'Commande créée',
        self::ORDER_PENDING => 'Commande en attente',
        self::ORDER_CANCELED => 'Commande annulée',
        self::ORDER_VALIDATED => 'Commande validée',
        self::ORDER_SENT => 'Commande envoyée',
        self::ORDER_REPAID => 'Commande remboursée'
    ];

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * Get pic row.
     *
     *
     * @return Pic The pic
     */
    public function getUserOrder($id)
    {
        $sql = "SELECT * FROM user_order WHERE id=:id ";

        $sth = $this->db->prepare($sql);
        $sth->bindParam("id", $id);
        $sth->execute();
        $res = $sth->fetchObject();

        return $res;
    }

    /**
     * Get all orders row.
     *
     * @return Pic The pic
     */
    public function getAllUserOrder()
    {
        $sql = "SELECT * FROM user_order ORDER BY `id` DESC ";

        $sth = $this->db->prepare($sql);
        $sth->execute();
        $res = $sth->fetchAll();

        return $res;
    }

    /**
     * Get all pics row.
     *
     * @return Pic The pic
     */
    public function getAllOrderByUserId($userId)
    {
        $sql = "SELECT * FROM user_order WHERE user_id=:user_id ORDER BY `id` DESC ";

        $sth = $this->db->prepare($sql);
        $sth->execute(['user_id' => $userId]);
        $res = $sth->fetchAll();

        return $res;
    }

    /**
     * Insert order row.
     *
     * @param array $order The order
     *
     * @return int The new ID
     */
    public function insertUserOrder(array $userOrder): int
    {
        $sql = "INSERT INTO user_order SET 
                status=:status,
                amount=:amount,
                user_id=:user_id,
                created_at=NOW(),
                last_updated_at=NOW();";

        $this->db->prepare($sql)->execute($userOrder);

        return (int)$this->db->lastInsertId();
    }

    /**
     * Update order row.
     *
     * @param array $order The order
     *
     */
    public function updateUserOrder(array $userOrder, int $id)
    {
        $sql = "UPDATE user_order SET 
                status=:status,
                amount=:amount,
                user_id=:user_id,
                last_updated_at=NOW(),
                WHERE id = " . $id;

        return $this->db->prepare($sql)->execute($userOrder);
    }

    /**
     * set order price.
     *
     * @param array $order The order
     *
     */
    public function setAmount(array $args)
    {
        $sql = "UPDATE user_order SET 
                amount=:amount,
                last_updated_at=NOW()
                WHERE id = " . $args['id'];

        return $this->db->prepare($sql)->execute(['amount' => $args['amount']]);
    }

    /**
     * Insert order row.
     *
     * @param array $order The order
     *
     * @return int The new ID
     */
    public function DeleteUserOrder($id): int
    {
        $sql = "UPDATE user_order SET
        last_updated_at=NOW(),
        deleted_at=NOW()
        WHERE id=:id ";

        return $this->db->prepare($sql)->execute(['id' => $id]);
    }

    public static function getOrderStatusLabel($status_id)
    {
        return self::ORDER_STATUS_LABEL[$status_id];
    }

    public static function getOrderStatusLabelPill($status)
    {

        switch ($status) {

            case self::ORDER_CREATED:
                $res = '<span class="badge badge-pill badge-danger">';
                break;
            case self::ORDER_PENDING:
                $res = '<span class="badge badge-pill badge-warning">';
                break;
            case self::ORDER_CANCELED:
                $res = '<span class="badge badge-pill badge-success">';
                break;
            case self::ORDER_VALIDATED:
                $res = '<span class="badge badge-pill badge-info">';
                break;
            case self::ORDER_SENT:
                $res = '<span class="badge badge-pill badge-success">';
                break;
            case self::ORDER_REPAID:
                $res = '<span class="badge badge-pill badge-success">';
                break;
            default:
                $res = '<span class="badge badge-pill badge-info">';
                break;
        }

        return $res . self::getOrderStatusLabel($status) . '</span>';;
    }
}
