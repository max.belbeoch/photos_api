<?php

namespace App\Repository;

use PDO;

/**
 * Repository.
 */
class PictureParameterRepository
{
    /**
     * @var PDO The database connection
     */
    private $db;

    const PRINT = [
        0 => 'Papier photo standard', 
        1 => 'Alu Dibond',
    ];

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * Get pic row.
     *
     *
     * @return Pic The pic
     */
    public function getPictureParameter($id)
    {
        $sql = "SELECT * FROM image_parameter WHERE id=:id ";

        $sth = $this->db->prepare($sql);
        $sth->bindParam("id", $id);
        $sth->execute();
        $res = $sth->fetchObject();

        return $res;
    }

    /**
     * Get all pics row.
     *
     * @return Pic The pic
     */
    public function getAllPictureParameter()
    {
        $sql = "SELECT * FROM image_parameter ORDER BY `id` DESC ";

        $sth = $this->db->prepare($sql);
        $sth->execute();
        $res = $sth->fetchAll();

        return $res;
    }

    /**
     * Insert picture_parameter row.
     *
     * @param array $picture The picture
     *
     * @return int The new ID
     */
    public function insertPictureParameter(array $pictureParameter): int
    {
        $sql = "INSERT INTO image_parameter SET 
                print=:print,
                width=:width,
                height=:height, 
                price=:price,
                label=:label;";

        $this->db->prepare($sql)->execute($pictureParameter);

        return (int)$this->db->lastInsertId();
    }

    /**
     * Insert picture row.
     *
     * @param array $picture The picture
     *
     */
    public function updatePictureParameter(array $pictureParameter, int $id)
    {
        $sql = "UPDATE image_parameter SET 
                print=:print,
                width=:width,
                height=:height, 
                price=:price,
                label=:label
                WHERE id = " . $id;

        return $this->db->prepare($sql)->execute($pictureParameter);
    }

    /**
     * Insert picture row.
     *
     * @param array $picture The picture
     *
     * @return int The new ID
     */
    public function DeletePictureParameter($id): int
    {
        $sql = "DELETE FROM image_parameter WHERE id=:id ";

        return $this->db->prepare($sql)->execute(['id' => $id]);
    }

    public static function getPrintLabel($id){
        return self::PRINT[$id];
    }
}
