<?php

namespace App\Repository;

use PDO;

/**
 * Repository.
 */
class PictureItemRepository
{
    /**
     * @var PDO The database connection
     */
    private $db;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * Get items row.
     *
     *
     * @return Pic The pic
     */
    public function getPictureItems($id)
    {
        $sql = "SELECT * FROM image_item WHERE image_id=:image_id ORDER BY `image_parameter_id` ASC ";

        $sth = $this->db->prepare($sql);
        $sth->bindParam("image_id", $id);
        $sth->execute();
        $res = $sth->fetchAll();

        return $res;
    }

    /**
     * Get items row.
     *
     *
     * @return Pic The pic
     */
    public function getPictureItem($id)
    {
        $sql = "SELECT * FROM image_item WHERE id=:id ";

        $sth = $this->db->prepare($sql);
        $sth->bindParam("id", $id);
        $sth->execute();
        $res = $sth->fetchObject();

        return $res;
    }

    /**
     * Get items row.
     *
     *
     * @return Pic The pic
     */
    public function getPictureItemByParamId($id, $parameter_id)
    {
        $sql = "SELECT * FROM image_item WHERE image_id=:image_id AND image_parameter_id=:image_parameter_id ";

        $sth = $this->db->prepare($sql);
        $sth->bindParam("image_id", $id);
        $sth->bindParam("image_parameter_id", $parameter_id);
        $sth->execute();
        $res = $sth->fetchObject();

        return $res;
    }

    /**
     * Get items row.
     *
     *
     * @return Pic The pic
     */
    public function hasItem($id)
    {
        $sql = "SELECT COUNT(*) FROM image_item WHERE image_id=:image_id ";

        $sth = $this->db->prepare($sql);
        $sth->bindParam("image_id", $id);
        $sth->execute();
        $res = $sth->fetchColumn()[0];
        
        return $res ? $res : false ;
    }

    /**
     * Get all pics row.
     *
     * @return Pic The pic
     */
    public function getAllPictureItem()
    {
        $sql = "SELECT * FROM image ORDER BY `id` DESC ";

        $sth = $this->db->prepare($sql);
        $sth->execute();
        $res = $sth->fetchAll();

        return $res;
    }

    /**
     * Insert picture_item row.
     *
     * @param array $picture The picture
     *
     * @return int The new ID
     */
    public function insertPictureItem(array $pictureItem): int
    {
        $sql = "INSERT INTO image_item SET 
                image_id=:image_id, 
                image_parameter_id=:image_parameter_id;
                ";

        $this->db->prepare($sql)->execute($pictureItem);

        return (int)$this->db->lastInsertId();
    }

    /**
     * Insert picture row.
     *
     * @param array $picture The picture
     *
     * @return int The new ID
     */
    public function DeletePictureItem($id): int
    {
        $sql = "DELETE FROM image_item WHERE id=:id ";

        return $this->db->prepare($sql)->execute(['id' => $id]);
    }
}
