<?php

namespace App\Repository;

use PDO;

/**
 * Repository.
 */
class PictureRepository
{
    /**
     * @var PDO The database connection
     */
    private $db;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * Get pic row.
     *
     *
     * @return Pic The pic
     */
    public function getPicture($id)
    {
        $sql = "SELECT * FROM image WHERE id=:id AND `deleted_at` IS NULL ";

        $sth = $this->db->prepare($sql);
        $sth->bindParam("id", $id);
        $sth->execute();
        $res = $sth->fetchObject();

        return $res;
    }

    /**
     * Get all pics row.
     *
     * @return Pic The pic
     */
    public function getAllPictures()
    {
        $sql = "SELECT * FROM image WHERE `deleted_at` IS NULL ORDER BY `id` DESC ";

        $sth = $this->db->prepare($sql);
        $sth->execute();
        $res = $sth->fetchAll();

        return $res;
    }

    /**
     * Get all pics row.
     *
     * @return Pic The pic
     */
    public function getAllPicturesToSell()
    {
        $sql = "SELECT * FROM image WHERE `to_sell` = 1 AND `deleted_at` IS NULL ORDER BY `id` DESC ";

        $sth = $this->db->prepare($sql);
        $sth->execute();
        $res = $sth->fetchAll();

        return $res;
    }

    public function getPortfolioContent()
    {
        $sql = 'SELECT * FROM image WHERE `note` > 3 AND `deleted_at` IS NULL ORDER BY RAND() DESC LIMIT 0,9';

        $sth = $this->db->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllAlbums()
    {
        $sql = 'SELECT * FROM album';

        $sth = $this->db->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllAlbumToSell()
    {
        $sql = 'SELECT DISTINCT(album_id) FROM image where `to_sell` = 1';

        $sth = $this->db->prepare($sql);
        $sth->execute();
        $albumsId = $sth->fetchAll(PDO::FETCH_ASSOC);

        $albums = [];
        foreach ($albumsId as $id) {
            $sql = 'SELECT * FROM album where `id` = ' . $id['album_id'];
            $sth = $this->db->prepare($sql);
            $sth->execute();
            $albums[] = $sth->fetchAll(PDO::FETCH_ASSOC)[0];
        }
        return $albums;
    }

    public function albumExists($id)
    {
        $sql = 'SELECT `nom` FROM album WHERE `id` = ' . $id;

        $sth = $this->db->prepare($sql);
        $sth->execute();

        if (empty($sth->fetchAll(PDO::FETCH_ASSOC))) {
            return false;
        } else {
            return true;
        }
    }

    public function getAlbumNbImg($id)
    {
        $sql = 'SELECT COUNT(*) FROM image WHERE `album_id` = ' . $id . ' AND `deleted_at` IS NULL';

        $sth = $this->db->prepare($sql);
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_COLUMN)[0] / 9;
    }

    public function getAlbumName($id)
    {
        $sql = 'SELECT nom FROM album WHERE `id` = ' . $id;

        $sth = $this->db->prepare($sql);
        $sth->execute();

        return $sth->fetchColumn();
    }

    public function getAlbumImg($albumId, $params)
    {

        $sql = 'SELECT * FROM image WHERE `deleted_at` IS NULL AND `album_id` = ' . $albumId . ' ORDER BY `id` DESC ';

        if ($params['pageNum'] > 1) {
            $sql .= ' LIMIT ' . ((int) $params['pageNum'] - 1) * 9 . ', 9' ;
        } else {
            $sql .= ' LIMIT 9 ';
        }

        $sth = $this->db->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getLastPicId()
    {
        $sql = 'SELECT MAX(id) FROM image';

        $sth = $this->db->prepare($sql);
        $sth->execute();

        return $sth->fetch(PDO::FETCH_COLUMN);
    }

    /**
     * Insert picture row.
     *
     * @param array $picture The picture
     *
     * @return int The new ID
     */
    public function insertPicture(array $picture): int
    {
        $sql = "INSERT INTO image SET 
                nom=:nom, 
                nom_photo=:nom_photo, 
                nom_photo_min=:nom_photo_min, 
                album_id=:album_id,
                localisation=:localisation,
                note=:note,
                to_sell=:to_sell,
                description=:description,
                created_at=NOW(),
                last_updated_at=NOW()";

        $this->db->prepare($sql)->execute($picture);

        return (int)$this->db->lastInsertId();
    }

    /**
     * Insert picture row.
     *
     * @param array $picture The picture
     *
     */
    public function updatePicture(array $picture, int $id)
    {
        $sql = "UPDATE image SET 
                nom=:nom, 
                album_id=:album_id,
                localisation=:localisation,
                note=:note,
                to_sell=:to_sell,
                description=:description,
                last_updated_at=NOW()
                WHERE id = " . $id;

        return $this->db->prepare($sql)->execute($picture);
    }

    /**
     * Insert picture row.
     *
     * @param array $picture The picture
     *
     * @return int The new ID
     */
    public function DeletePicture($id): int
    {
        $sql = "UPDATE image SET
                last_updated_at=NOW(),
                deleted_at=NOW()
                WHERE id=:id ";

        return $this->db->prepare($sql)->execute(['id' => $id]);
    }
}
