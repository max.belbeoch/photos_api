<?php

namespace App\Repository;

use PDO;

/**
 * Repository.
 */
class OrderItemRepository
{
    /**
     * @var PDO The database connection
     */
    private $db;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * Get pic row.
     *
     *
     * @return Pic The pic
     */
    public function getOrderItem($id)
    {
        $sql = "SELECT * FROM order_item WHERE id=:id ";

        $sth = $this->db->prepare($sql);
        $sth->bindParam("id", $id);
        $sth->execute();
        $res = $sth->fetchObject();

        return $res;
    }

    /**
     * Get all pics row.
     *
     * @return Pic The pic
     */
    public function getAllOrderItem()
    {
        $sql = "SELECT * FROM order_item ORDER BY `id` DESC ";

        $sth = $this->db->prepare($sql);
        $sth->execute();
        $res = $sth->fetchAll();

        return $res;
    }

    /**
     * Insert picture_parameter row.
     *
     * @param array $picture The picture
     *
     * @return int The new ID
     */
    public function insertOrderItem(array $orderItem): int
    {
        $sql = "INSERT INTO order_item SET 
                image_item_id=:image_item_id,
                order_id=:order_id,
                quantity=:quantity;";

        $this->db->prepare($sql)->execute($orderItem);

        return (int)$this->db->lastInsertId();
    }

    /**
     * Insert picture row.
     *
     * @param array $picture The picture
     *
     */
    public function updateOrderItem(array $orderItem, int $id)
    {
        $sql = "UPDATE order_item SET 
                image_item_id=:image_item_id,
                order_id=:order_id,
                quantity=:quantity
                WHERE id = " . $id;

        return $this->db->prepare($sql)->execute($orderItem);
    }

    /**
     * Insert picture row.
     *
     * @param array $picture The picture
     *
     * @return int The new ID
     */
    public function DeleteOrderItem($id): int
    {
        $sql = "DELETE FROM order_item WHERE id=:id ";

        return $this->db->prepare($sql)->execute(['id' => $id]);
    }

}
