<?php

namespace App\Middleware;

use Exception;
use Firebase\JWT\JWT;
use Odan\Session\SessionInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Routing\RouteContext;

final class UserAuthApiMiddleware implements MiddlewareInterface
{
    /**
     * @var ResponseFactoryInterface
     */
    private $responseFactory;

    /**
     * @var SessionInterface
     */
    private $session;

    private $container;

    public function __construct(ResponseFactoryInterface $responseFactory, SessionInterface $session, ContainerInterface $container)
    {
        $this->responseFactory = $responseFactory;
        $this->container = $container;
        $this->session = $session;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {

        $data = $request->getHeader("Authorization");
        $settings = $this->container->get('settings');

        if (isset($data) && !empty($data)) {
            try {
                JWT::decode($data[0], $settings['jwt']['secret'], ['HS256']);
            } catch (Exception $e) {
                self::sendError('access denied token');
            }
        }

        return $handler->handle($request);
    }

    static function sendError($error)
    {
        $result = [
            'result' => false,
            'error' => $error,
        ];

        http_response_code(401);
        echo json_encode($result);
        exit;
    }
}
