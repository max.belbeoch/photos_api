<?php

namespace App\Service;

use App\Repository\PictureRepository;
use App\Service\PictureResizeService;
use Slim\Routing\RouteContext;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;

/**
 * Service.
 */
final class PictureService
{
    /**
     * @var UserRepository
     */
    private $repository;

    private $container;
    private $base_url;

    /**
     * The constructor.
     *
     * @param UserRepository $repository The repository
     */
    public function __construct(PictureRepository $repository, ContainerInterface $container)
    {
        $this->repository = $repository;
        $this->container = $container;
        $this->base_url = 'http://' . $_SERVER['HTTP_HOST'];
    }

    public function getPicture($id, $only_data = false)
    {
        $pic = $this->repository->getPicture($id);

        if (!$pic) {
            return false;
        }

        if ($only_data) {
            return $pic;
        }

        $args = [
            'id' => $pic->id,
            'min' => 1,
            'get_64' => 1
        ];

        $pic->base_64_min = $this->getImgResized($args);

        return $pic;
    }

    public function getPictureToSell($request, $id)
    {
        $pic = $this->repository->getPicture($id);

        if (!$pic) {
            return false;
        }

        $routeParser = RouteContext::fromRequest($request)->getRouteParser();
        $pic->link = $this->base_url . $routeParser->urlFor('photo', ['id' => $id, 'min' => 0, 'get_64' => 0, 'to_display' => 1]);

        return $pic;
    }

    public function getAllPicturesToSell($request)
    {
        $pics = $this->repository->getAllPicturesToSell();

        foreach ($pics as &$pic) {
            $routeParser = RouteContext::fromRequest($request)->getRouteParser();
            $pic['link'] = $this->base_url . $routeParser->urlFor('photo', ['id' => $pic['id'], 'min' => 1, 'get_64' => 0, 'to_display' => 0]);
        }

        return $pics;
    }

    public function getAlbumToSell($request, $id)
    {
        $album = $this->repository->getAlbumToSell($id);

        if (!$album) {
            return false;
        }

        $routeParser = RouteContext::fromRequest($request)->getRouteParser();
        $album->link = $this->base_url . $routeParser->urlFor('photo', ['id' => $id, 'min' => 0, 'get_64' => 0, 'to_display' => 1]);

        return $album;
    }

    public function getAllAlbumToSell($request)
    {
        $albums = $this->repository->getAllAlbumToSell();

        foreach ($albums as &$album) {
            $routeParser = RouteContext::fromRequest($request)->getRouteParser();
            $album['link'] = $this->base_url . '/photos_api/assets/picAlbums/' . $album['nom']  . '.jpg';
        }

        return $albums;
    }

    public function getImgResized($args)
    {

        if (!(isset($args['id']) && is_numeric((int) $args['id']))) {
            exit;
        }

        if (isset($args['get_64']) && !empty($args['get_64']) && $args['get_64'] == 1) {        // TO CHECK
            $get64 = true;
        } else {
            $get64 = false;
        }

        if (isset($args['to_display']) && !empty($args['to_display']) && $args['to_display'] == 1) {        // TO CHECK
            $toDisplay = true;
        } else {
            $toDisplay = false;
        }

        // resize to implement for base_64
        if (isset($args['min']) && !empty($args['min']) && $args['min'] == 1) {
            $photoName = 'nom_photo_min';
        } else {
            $photoName = 'nom_photo';
        }

        $photoid = $args['id'];

        $image = $this->repository->getPicture($photoid);

        if (!$image) {
            exit;
        }

        $imgPath = __DIR__ . '/../../public/assets/upload/' . $image->$photoName;

        if (!is_file($imgPath)) {
            exit;
        }

        if ($get64) {
            $img_data = file_get_contents($imgPath);
            $data = base64_encode($img_data);

            if ($data != null) {
                if ($toDisplay) {
                    echo $data;
                    exit;
                }
                return $data;
            }
        }

        header('Content-Type: image/jpeg');

        $image = imagecreatefromjpeg($imgPath);

        if ($image === false) {
            exit;
        }

        $plus_grande_des_tailles = max(imagesx($image), imagesy($image));

        $taille_maximum_autorisee = 3000;

        $nouvelle_largeur = imagesx($image) * $taille_maximum_autorisee / $plus_grande_des_tailles;
        $nouvelle_hauteur = imagesy($image) * $taille_maximum_autorisee / $plus_grande_des_tailles;

        $image_redimensionnee = imagecreatetruecolor($nouvelle_largeur, $nouvelle_hauteur);
        imagecopyresized($image_redimensionnee, $image, 0, 0, 0, 0, $nouvelle_largeur, $nouvelle_hauteur, imagesx($image), imagesy($image));

        imagejpeg($image_redimensionnee);

        imagedestroy($image);
        imagedestroy($image_redimensionnee);
    }
}
