<?php

namespace App\Service;

use App\Repository\UserRepository;
use App\Exception\ValidationException;
use Exception;
use Psr\Container\ContainerInterface;
use \Firebase\JWT\JWT;

/**
 * Service.
 */
final class UserService
{
    /**
     * @var UserRepository
     */
    private $repository;

    private $container;

    /**
     * The constructor.
     *
     * @param UserRepository $repository The repository
     */
    public function __construct(UserRepository $repository, ContainerInterface $container)
    {
        $this->repository = $repository;
        $this->container = $container;
    }

    /**
     * Create a new user.
     *
     * @param array $data The form data
     *
     * @return int The new user ID
     */
    public function createUser(array $data): int
    {
        // Input validation
        $this->validateNewUser($data);

        // Insert user
        $userId = $this->repository->insertUser($data);

        return $userId;
    }

    /**
     * Log a user.
     *
     * @param array $data The form data
     *
     * @return string The token
     */
    public function loginUser(array $data, $isAdmin = false)
    {
        // Input validation
        $this->validateLoginUser($data);

        $user = $this->repository->getUser($data, false, $isAdmin);

        // verify email address.
        if (!$user) {
            return false;
        }
        // verify password.
        if (!password_verify($data['password'], $user->password)) {
            return false;
        }

        $settings = $this->container->get('settings'); // get settings array.

        $token = JWT::encode(['id' => $user->id, 'email' => $user->email], $settings['jwt']['secret'], "HS256");

        $args = [
            'token' => $token,
            'id' => $user->id
        ];

        $user = $this->repository->setUserToken($args);

        return $token;

    }

    public function getUserByToken($request){
        
        $data = $request->getHeader("Authorization");
        $settings = $this->container->get('settings');

        if (isset($data) && !empty($data)) {
            try {
                $decoded = JWT::decode($data[0], $settings['jwt']['secret'], ['HS256']);
                return $this->repository->getUser([ 'email' => $decoded->email]);
            } catch (Exception $e) {
                return false;
            }
        }
    }

    public function isLogged($request){

        $data = $request->getHeader("Authorization");
        $settings = $this->container->get('settings');

        if (isset($data) && !empty($data)) {
            try {
                $decoded = JWT::decode($data[0], $settings['jwt']['secret'], ['HS256']);
            } catch (Exception $e) {
                return false;
            }

            $user = $this->repository->getUser([ 'email' => $decoded->email]);
            $now = strtotime(date('Y-m-d H:i:s'));
            $expirationDate = strtotime($user->token_expiration_date);

            if($now > $expirationDate){
                return false;
            }

            return true;
        }

        return false;;
    }

    /**
     * Input validation.
     *
     * @param array $data The form data
     *
     * @throws ValidationException
     *
     * @return void
     */
    private function validateNewUser(array $data): void
    {
        $errors = [];

        // Here you can also use your preferred validation library

        if (empty($data['password'])) {
            $errors['password'] = 'Input required';
        }

        if (empty($data['email'])) {
            $errors['email'] = 'Input required';
        } elseif (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
            $errors['email'] = 'Invalid email address';
        }

        if ($errors) {
            throw new ValidationException('Please check your input', $errors);
        }
    }

    /**
     * Input validation.
     *
     * @param array $data The form data
     *
     * @throws ValidationException
     *
     * @return void
     */
    private function validateLoginUser(array $data): void
    {
        $errors = [];

        // Here you can also use your preferred validation library

        if (empty($data['password'])) {
            $errors['password'] = 'Input required';
        }

        if (empty($data['email'])) {
            $errors['email'] = 'Input required';
        } elseif (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
            $errors['email'] = 'Invalid email address';
        }

        if ($errors) {
            //throw new ValidationException('Please check your input', $errors);
        }
    }

}
