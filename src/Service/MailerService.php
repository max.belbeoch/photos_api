<?php

namespace App\Service;

use Exception;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\BodyRendererInterface;

final class MailerService
{
    /**
     * @var MailerInterface
     */
    private $mailer;
    private $bodyRenderer;

    public function __construct(MailerInterface $mailer, BodyRendererInterface $bodyRenderer)
    {
        $this->mailer = $mailer;
        $this->bodyRenderer = $bodyRenderer;
    }

    public function sendSignupMail($user)
    {
        $datas = [
            'from' => 'contact@maxime-belbeoch.com',
            'to' => $user->email,
            'subject' => 'Création de compte',
            'text' => 'mail text',
            'template' => 'emails/signup.html.twig',
            'content' => [
                'full_name' => $user->first_name,
                'code' => uniqid()
            ]
        ];

        $this->sendEmail($datas);

        return $datas;
    }

    public function sendEmail(array $datas): void
    {
       
        // Send email
        $email = (new TemplatedEmail())
            ->from($datas['from'])
            ->to(new Address($datas['to']))
            ->subject($datas['subject'])
            ->text($datas['text'])
            ->htmlTemplate($datas['template'])
            ->context($datas['content']);

        // Render the email twig template
        $this->bodyRenderer->render($email);

        try {
            $this->mailer->send($email);
        } catch (Exception $e) {
           exit(print_r($e->getMessage(), true));
        }
    }

}
