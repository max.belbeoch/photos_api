<?php

use Slim\App;
use Slim\Middleware\ErrorMiddleware;
use Selective\BasePath\BasePathMiddleware;
use Odan\Session\Middleware\SessionMiddleware;
use App\Middleware\CorsMiddleware;
use Slim\Views\TwigMiddleware;

return function (App $app) {

    // Parse json, form data and xml
    $app->addBodyParsingMiddleware();

    // Cors gesture (API)
    $app->add(CorsMiddleware::class);

    $app->add(TwigMiddleware::class);
    
    // Start the session
    $app->add(SessionMiddleware::class);
    $app->addRoutingMiddleware();

    // Add the Slim built-in routing middleware
    $app->addRoutingMiddleware();

    $app->add(BasePathMiddleware::class);

    // Catch exceptions and errors
    $app->add(ErrorMiddleware::class);

};
