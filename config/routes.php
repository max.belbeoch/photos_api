<?php

use Slim\App;
use Slim\Routing\RouteCollectorProxy;
use App\Middleware\UserAuthMiddleware;
use App\Middleware\UserAuthApiMiddleware;
use App\Action\PreflightAction;
use App\Action\PortfolioAction;
use App\Action\HomeAction;
use App\Action\ContactAction;
use App\Action\AlbumsAction;
use App\Action\AlbumAction;
use App\Action\ProposAction;
use App\Action\photoAction;
use App\Action\User\UserRegisterAction;
use App\Action\User\UserLoginAction;
use App\Action\User\UserLogoutAction;
use App\Action\User\IsLoggedAction;
use App\Action\Api\PictureGetOneAction;
use App\Action\Api\PictureGetAllAction;
use App\Action\Api\AlbumGetAllAction;
use App\Action\Api\AlbumGetOneAction;
use App\Action\Api\OrderCreateAction;
use App\Action\Api\OrderGetAllAction;
use App\Action\Api\OrderGetOneAction;
use App\Action\Admin\AdminLogoutAction;
use App\Action\Admin\AdminHomeAction;
use App\Action\Admin\AdminLoginAction;
use App\Action\Admin\AdminOrderAction;
use App\Action\Admin\AdminShopAction;
use App\Action\Admin\Picture\AdminAddPictureAction;
use App\Action\Admin\Picture\AdminUpdatePictureAction;
use App\Action\Admin\Picture\AdminDeletePictureAction;
use App\Action\Admin\PictureParameter\AdminAddPictureParameterAction;
use App\Action\Admin\PictureParameter\AdminUpdatePictureParameterAction;
use App\Action\Admin\PictureParameter\AdminDeletePictureParameterAction;
use App\Action\Admin\PictureParameter\AdminSetPictureItemAction;
use App\Action\Admin\PictureParameter\AdminPictureParameterGetOneAction;
use App\Action\Admin\PictureParameter\AdminPictureItemGetAllAction;

return function (App $app) {

    // Site Routes
    $app->get('/', HomeAction::class)->setName('home');
    $app->get('/portfolio', PortfolioAction::class)->setName('portfolio');
    $app->any('/contact', ContactAction::class)->setName('contact');
    $app->get('/albums', AlbumsAction::class)->setName('albums');
    $app->get('/album/{id}[/{pageNum}]', AlbumAction::class)->setName('album');
    $app->get('/propos', ProposAction::class)->setName('propos');

    // Pic resize
    $app->get('/photo/{id}/{get_64}/{min}/{to_display}', PhotoAction::class)->setName('photo');

    /*
    // API Routes
    $app->group('/api', function (RouteCollectorProxy $group) {

        $group->options('/login', PreflightAction::class);
        $group->options('/logout', PreflightAction::class);
        $group->options('/register', PreflightAction::class);
        $group->options('/pictures', PreflightAction::class);
        $group->options('/orders', PreflightAction::class);
        $group->options('/order/create', PreflightAction::class);
        $group->options('/is_logged', PreflightAction::class);

        $group->post('/register', UserRegisterAction::class)->setName('register');
        $group->post('/login', UserLoginAction::class)->setName('login');
        $group->get('/logout', UserLogoutAction::class)->setName('logout');
        $group->get('/is_logged', IsLoggedAction::class)->setName('is_logged');

        $group->get('/albums', AlbumGetAllAction::class)->setName('api_albums');
        $group->any('/album/{id}', AlbumGetOneAction::class)->setName('api_album');
        $group->get('/pictures', PictureGetAllAction::class)->setName('api_pictures');
        $group->any('/picture/{id}', PictureGetOneAction::class)->setName('api_picture');
        $group->post('/user/delete/{id}', UserDeleteAction::class)->setName('delete');

        $group->get('/orders', OrderGetAllAction::class)->setName('orders')->add(UserAuthApiMiddleware::class);
        $group->post('/order/create', OrderCreateAction::class)->setName('order_create')->add(UserAuthApiMiddleware::class);
        $group->any('/order/{id}', OrderGetOneAction::class)->setName('order')->add(UserAuthApiMiddleware::class);
        //$group->post('/user/delete/{id}', UserDeleteAction::class)->setName('delete');
    });
*/
    // Admin
    $app->any('/admin/login', AdminLoginAction::class)->setName('admin_login');
    $app->group('/admin', function (RouteCollectorProxy $group) {
        $group->get('/logout', AdminLogoutAction::class)->setName('admin_logout');
        $group->get('/home', AdminHomeAction::class)->setName('admin_home');
        $group->get('/shop', AdminShopAction::class)->setName('admin_shop');
        $group->get('/orders', AdminOrderAction::class)->setName('admin_order');
        //$group->any('/order/{id}', AdminShopAction::class)->setName('admin_order');
        $group->post('/order/add', AdminAddOrderAction::class)->setName('add_order');
        $group->post('/order/update', AdminUpdateOrderAction::class)->setName('update_order');
        $group->post('/order/delete', AdminDeleteOrderAction::class)->setName('delete_order');
        $group->post('/picture/add', AdminAddPictureAction::class)->setName('add_picture');
        $group->post('/picture/update', AdminUpdatePictureAction::class)->setName('update_picture');
        $group->post('/picture/delete', AdminDeletePictureAction::class)->setName('delete_picture');
        $group->post('/picture_parameter/add', AdminAddPictureParameterAction::class)->setName('add_picture_parameter');
        $group->post('/picture_parameter/update', AdminUpdatePictureParameterAction::class)->setName('update_picture_parameter');
        $group->post('/picture_parameter/delete', AdminDeletePictureParameterAction::class)->setName('delete_picture_parameter');
        $group->any('/picture_parameter/{id}', AdminPictureParameterGetOneAction::class)->setName('get_picture_parameter');
        $group->any('/picture_items/{id}', AdminPictureItemGetAllAction::class)->setName('get_picture_items');
        $group->post('/picture_item/set', AdminSetPictureItemAction::class)->setName('set_picture_item');
    })->add(UserAuthMiddleware::class);
};
